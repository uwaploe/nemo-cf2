/* first-in, last-out dynamic queue, grows & shrinks when called */

#include	<ctype.h>
#include	<errno.h>
#include	<math.h>
#include	<stddef.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>


void deQueue(int *nf,char *fn);
int enQueue(char *fname);
void initQueue(void);


struct fileQ
{
	int fileCount; // count of files in the queue, increment by 1 each time a 
				   // file is queued.  max is 32768 and also limited by memory
	char fileName[13];
	struct fileQ *lastFile;
	struct fileQ *nextFile;
};

static struct fileQ *fq=NULL;
static struct fileQ *lastFq;
	

/* push a file onto the queue. */ 
int enQueue(
char *fname)
{
int nf;

	nf=fq->fileCount;
	if(nf > 32766) return -1;  // queue full
	//printf("# in q: %d\n",nf);
	lastFq=fq;
	
	fq=fq->nextFile;
	// allocate space for the next object
	fq=(struct fileQ *)malloc(sizeof( struct fileQ ) );				
	if(fq == NULL) return -1; // out of memory - unlikely since we are enqueuing &
							  // dequeuing too.
	strcpy(fq->fileName,fname);
	fq->fileCount= ++nf;
	fq->lastFile=lastFq;
	//printf("enQueue(): file %d added : %s\n",fq->fileCount,fq->fileName);
	return nf;
}


/* pop a file from the queue and free memory as well */
void deQueue(
int *nf,  // return the number of files in queue at the time of this call
char *fn)
{
	
	if(fq->lastFile != NULL) 
	{
		lastFq=fq; // save ptr for freeing memory
		strcpy(fn,fq->fileName);
		*nf=fq->fileCount;
		fq=fq->lastFile;
		// de-allocate space for the current de-queued object
		free(lastFq);
	}
	else
	{
		//strcpy(fn,"");
		fn=NULL;
		*nf=0;
	}
}


void initQueue(void)
{
	// 1st queue is blank/NULL
	fq=(struct fileQ *)malloc(sizeof( struct fileQ ) );
	fq->lastFile=NULL;	
	fq->fileCount=0;
}

