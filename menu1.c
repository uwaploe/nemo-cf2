/*
**$Id: menu.c%v 1.1 1994/10/29 01:34:35 mike Exp mike $
**
** Simple-minded menu display function
**
*/
#include	<cfxbios.h>		// Persistor CF1 System and I/O Definitions
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "menu.h"
#include "prototype.h"
#include "constant.h"

static
void display_menu(const char *title, register MenuEntry *menu, size_t n,
  const char *from)
{
    register int	i;
    int			quit = n + 1, slen;

    slen = strlen(title);
    
    putchar('\n');
    for(i = 0;i < slen+13;i++) putchar('-');
    printf("\n    %s\n", title);
    for(i = 0;i < slen+13;i++) putchar('-');
    putchar('\n');

    for(i = 0;i < n;i++)
	printf("%2d)  %s\n",i+1, menu[i].label);
	
#if 0
    if(from)
    {
        //printf("%2d)  %s\n", quit, from);
        printf("CR)  %s\n", from);
    }
    else
        printf("CR)  Return to main menu\n");
#endif

}


int show_menu(const char *title, register MenuEntry *menu, size_t n,
  const char *from)
{
    int		quit = n ; // exit on the last menu item
    short	sel_num = 0;
    char	prompt[40];



    do
    {
    sprintf(prompt, "Enter selection (1-%d): ", quit);
	display_menu(title, menu, n, from);
    sel_num= -1;  
    printf("%s",prompt);  fflush(stdout);
    consoleInput(prompt); // watchdog, if used, is kicked in consoleInput()
	if(strlen(prompt) >1) sel_num=atoi(prompt);
    if( sel_num < 1 || sel_num > n) continue;
    if(menu[sel_num-1].f)
            (*menu[sel_num-1].f)(menu[sel_num-1].call_data);
    } while(sel_num != quit);

    return 0;
}
