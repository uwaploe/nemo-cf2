#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<stddef.h>
#include	<time.h>
#include <ctype.h>	// for toupper() function

#include "prototype.h"
#include "constant.h"



int ADCPtxfer(
TUPort *tuport,
int *filesInQueue)
{
short i,result = 0;		// no errors so far
uchar cmdStr[80],dataBuf[2501],cTemp[60],fn[16];
short nBytesRead;
FILE *fp;
time_t nowSecs;

	// open file first
	nowSecs=time(NULL);
	strftime(cmdStr, sizeof(cmdStr)-1, "%y%j%H%M", localtime(&nowSecs));
	sprintf(fn,"%s.%s",&cmdStr[1],FN_SUFFIX_ADCP);  // keep only the last digit of year

	// interrupt ADCP
	TUBreak(tuport,70);
	RTCDelayMicroSeconds(3500000L);		

	result=serDeviceChatCF2(tuport,true,"\r", ">",2000L,cTemp,0);
	result=serDeviceChatCF2(tuport,true,"\r", ">",2000L,cTemp,0);
	if(result < 1)
	{
		sprintf(cmdStr,"ADCPtxfer(): No comm with ADCP!\n");
		printf("%s",cmdStr);
		logMsg(cmdStr);
		return 0;		
	} 
	
	sprintf(cmdStr,"ce\r"); // download 
	TUTxPutBlock(tuport,cmdStr,strlen(cmdStr),1000);
	// get command echo & metadata
	nBytesRead=serBufReadTimed(tuport,dataBuf,2500,3500,true);
	if(nBytesRead == 0) 
	{
		sprintf(cmdStr,"ADCPtxfer(): No bytes downloaded from ADCP.\n");
		printf("%s",cmdStr);
		logMsg(cmdStr);
		return 0;		
	} 
	
	printf("ADCPtxfer(): file name: %s\n",fn);
	if((fp=fopen(fn,"wb")) == NULL)
	{
		sprintf(cmdStr,"ADCPtxfer(): Fopen(wb) error!\n");
		printf("%s",cmdStr);
		logMsg(cmdStr);
		return 0;		
	} 
	
	// do not write 'ce\r\n'
	fwrite(&dataBuf[4],1,nBytesRead-4,fp);
	fclose(fp);
	*filesInQueue=enQueue(fn);
	
	sprintf(cmdStr,"cs\r"); // resume logging, ADCP doesn't acknowledge
	TUTxPutBlock(tuport,cmdStr,strlen(cmdStr),1000);
	return 1;
}
