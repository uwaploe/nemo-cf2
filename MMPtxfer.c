#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<stddef.h>
#include <ctype.h>	// for toupper() function

#include "prototype.h"
#include "constant.h"

int	CheckSIMDataPktHdr(uchar *ptr, short *len);


/******************************************************************************/
// read data pkt header from PC.
// Returns 1 if CRC_ pkt
// Returns 2 if DAT_ header
// Returns 3 if EOD_ pkt
// else returns 0
// TW: data packet contains null characters, so can't use strstr() to search
int	CheckSIMDataPktHdr(uchar *ptr, short *nn)
{
	ushort	c,d,e,r,i,j,len;
	char	C[]={'C','R','C'};
	char	D[]={'D','A','T'};
	char	E[]={'E','O','D'};
	uchar *p;
	
	len=*nn;
	p=ptr;
	c=d=e=r=0;
	for(i=0;i<20;i++)
	{
		j=toupper(*p++);
		if(j==C[c])
		{
			if(++c == 3)
			{
				r=1;
				break;
			}
		}
		else c=0;
		
		if(j==D[d])
		{
			if(++d == 3)
			{
				r=2;
				break;
			}
		}
		else d=0;
		
		if(j==E[e])
		{
			if(++e == 3)
			{
				r=3;
				break;
			}
		}
		else e=0;
	}
	
	if(r==0) return(0);
// does buffer need to be shifted due to leading bogus char(s)?
	if(i>2)
	{
		j=i-2;
		len -= j;
		memmove(ptr,ptr+j, len);
		*nn=len;
//if(echolog) cprintf("CheckSIMData..  had to shift buffer %d chars\n",j);
	}
	return(r);
}


// IMMchat(): a wrapper function to talk to IMM, 
// returns 1 if expected string is received, else 0
int IMMchat(
TUPort *port,
bool clearBufferFirst,
char *send,     // string to write to the modem.
char *expect,   // response string to wait for, must be 0 or >= 2 and < 31 chs.
long timeout  // maximum number of ms to wait for 'expect' string
)
{
int i,result;
char cTemp[10],*p,send2[30];

	strcpy(send2,send);
	p=strchr(send2,'\r');
	*p=0;
	
	for(i=0; i< 5; i++)
	{
		result=serDeviceChatCF2(port,clearBufferFirst,send,expect,timeout,cTemp,0);
		if (result > 0) 
		{
			//printf("IMMchat(): cmd '%s' succeeded on loop %d...\n",send2,i);
			return 1;
		}
		RTCDelayMicroSeconds(1000000L);		
	}

	printf("IMMchat(): cmd '%s' failed!!!!\n",send2);
	return 0;
}


// --- IMM must be in ConfigType=2 to detect wakeup tone ----
// Termination character is \r\n  in ConfigType=2 in Host service mode.
int IMMsetConfigType2(
TUPort *tuport_imm)
{
int result;

	printf("In IMMsetConfigType2()...\n");

	// occasionally IMM continue to output garbage characters on its own.
	// an ESCAPE character will stop it
	TUTxPutByte(tuport_imm,0x1b,true); // note the byte is sent as 0x1b, not '0x1b' !

	// get a prompt
	result=IMMchat(tuport_imm,true,"\r\n",">",2000);
	if(result == 0) return 0;
	
	// 'SetConfigType=2' must be sent twice!
	result=IMMchat(tuport_imm,true,"SetConfigType=2\r\n","<ConfirmationRequired/>",2500);
	RTCDelayMicroSeconds(100000L);	// wait for the rest of reply	
	result=IMMchat(tuport_imm,true,"SetConfigType=2\r\n","<PowerOff/>",2000);
	// IMM in poweroff mode after setting config type,, wake up with a CR.
	result=IMMchat(tuport_imm,true,"\r\n","IMM>",1000);
	result=IMMchat(tuport_imm,true,"setEnableToneDetect=1\r\n","<Executed/>",1000);
	result=IMMchat(tuport_imm,true,"pwroff\r\n","<PowerOff/>",1000);
	return result;
}


// --- IMM must be in ConfigType=1 to send 'pwron' -----
// Termination character is \r  in ConfigType=1 in Host service mode.
int IMMsetConfigType1(
TUPort *tuport_imm)
{
int result;

	printf("In IMMsetConfigType1()...\n");
	
	// occasionally IMM continue to output garbage characters on its own.
	// an ESCAPE character will stop it
	TUTxPutByte(tuport_imm,0x1b,true); // note the byte is sent as 0x1b, not '0x1b' !
	
	// get a prompt
	result=IMMchat(tuport_imm,true,"\r\n","IMM>",2000);
	if(result == 0) return 0;

	// 'SetConfigType=1' must be sent twice!
	result=IMMchat(tuport_imm,true,"SetConfigType=1\r\n","<ConfirmationRequired/>",2500);
	result=IMMchat(tuport_imm,true,"SetConfigType=1\r\n","<PowerOff/>",1500);
	// once in configType=1, the termination character is \r !!!
	// IMM in poweroff mode after setting config type, wake it up...
	result=IMMchat(tuport_imm,true,"\r","S>",1000);
	result=IMMchat(tuport_imm,true,"setthost4=500\r","S>",1000);
	// send 'pwron; note: can't send it in configtype=2
	// IMM replies: 'sending wake up tone,wait five seconds'
	result=IMMchat(tuport_imm,true,"pwron\r","S>",6000);
	return result;
}


/* ----------------------------------------------------------------------------
MMPtxfer():
call this function after the wakeup tone from the MMP has been detected..
and IMM has been set to configType=1.  Termination character is \r !!

- MMP packets:

	typedef struct 
	{ 
	  char    fileName[13];       // filename.ext - followed by white space 
	  char    profileEndTime[20]; // mm dd yyyy hh mm ss - followed " 
	  char    mooringID[4];       // ### - followed by " 
	  char    byteCount[12];       // ###### - followed by " 
	  char    term[2]; 
	} metaDataStruct; 

	typedef struct 
	{ 
	  char  dataHdr[4];   // "DAT", �CRC� 
	  short byteCount; // of whole pkt 8 + pktdatasize 
	  short CRC;       // CRC 
	} packetHeader; 

- a typical file txfer session: (!! commands to MMP are in uppercase !!!)

	S>#01REQNEW
	E0000000.DAT 10 06 2010 10 42 59 002         440 >
	S>b01REQACK
	DAT 6XL???L??7AE?D	@dL?BN/DGD@dL?BKY
	AD?DL?Bd_?D.DdL?m=?D?DL?BjI&AD|DL?Bg?AD?@dL??^DAD|DdL?yS;CAD?L??Gh?D?@dL??P?
	L??Vb]AD?D?L??[OsADJBO?DdL??V/?DnDd????L????
	>b01REQACK
	CRC ?
	S>#01REQNEW
	C0000000.DAT 10 06 2010 10 42 59 002          19 >
	S>b01REQACK
	DAT ????
	>b01REQACK
	CRC ?
	S>#01REQNEWE
	ENDOFDAT.DAT 01 01 1970 00 01 48 002           0 >
	S>#01REQNEW
	
- protocol during 2-way comm:
	IMM#1 captures the line
	IMM#1 sends a command
	IMM#2 decodes the command and waits
	IMM#1 releases the line
	a brief dead time (10mS I think?)
	IMM#2 captures the line
	IMM#2 sends a reply to IMM#1
	IMM#2 releases the line
	
---------------------------------------------------------------------------- */
int MMPtxfer(
TUPort *tuport,
int *filesInQueue)
{
short	i,result = 0,upldRet;		// no errors so far
uchar cmdStr[80],dataBuf[5000],fn[16],tmpStr[80],fnameGz[16];
int nBytesRead,loops,bytesInPacket,fpWriteError=false;
ushort CRCcomputed,CRCfromMMP,CRCmismatchCount;
FILE *fp;
char *p;	
RTCTimer tt;
time_t nowSecs;
		
	// loop to read all new files
	while(1)
	{
		strcpy(cmdStr,"#01REQNEW\r");
		while(1) // need to put a limit on the iterations !!!!
		{
			RTCDelayMicroSeconds(500000L);
			printf("\nsending '#01REQNEW'...\n"); // uppercase!!!
			/* MMP should respond with:
			#01REQNEW
			
			A0000086.DAT 01 14 2011 15 30 42 002       32768 >  
			
			S>
			or, if MMP not ready:
			'FAILED' msg='No reply from remote device'
			*/
			RTCElapsedTimerSetup(&tt);
			TUTxPutBlock(tuport,cmdStr,strlen(cmdStr),1000);
			// get command echo & metadata. Must enable "file deletion" on MMP
			// or else the response dely will get bigger and bigger with more profiles!!
			result=serBufReadTillExpectedStringTimed(tuport,dataBuf,"S>",10000,false);
			printf("result=%d, %s (%ld ms)\n",result,dataBuf,RTCElapsedTime(&tt)/1000);
			if(strstr(dataBuf,"ENDOFDAT.DAT")) // check this first!!!
			{
				RTCDelayMicroSeconds(2000000L); // wait for UIM to switch to receive mode
				strcpy(cmdStr,"#01REQEOD\r");
				TUTxPutBlock(tuport,cmdStr,strlen(cmdStr),1500);
				printf("...ENDOFDAT.DAT received, #01REQEOD sent...\n");
				goto NEXT_PROFILE;
			} 
			else if(strstr(dataBuf,".DAT")) break; // there is new file
			else //if(strstr(dataBuf,"'FAILED' msg='No reply from remote device'")) 
			{
				// comm problem, bail out.  will get garbage characters from IMM.
				sprintf(tmpStr,"MMPtxfer(): Unexpected responses to '#01REQNEW' from MMP/UIM.\n");
				printf("%s",tmpStr);
				logMsg(tmpStr);
				result=0;
				goto NEXT_PROFILE;
			}
		}
		
		// extract file name
		p=strstr(dataBuf,".DAT");
		strncpy(fn,p-8,12);
		fn[12]=0;
		if((fp=fopen(fn,"wb")) == NULL) 
			printf("MMPtxfer(): Fopen(wb) error!\n");
		else
		{
			printf("...file: %s opened ...\n",fn);
			fpWriteError=false;
		}

		
		RTCDelayMicroSeconds(500000L);
		strcpy(cmdStr,"b01REQACK\r");		
		result=IMMchat(tuport,true,cmdStr,cmdStr,5000);
		TURxFlush(tuport);

		CRCmismatchCount=false;
		// here comes the data - loop for each file until CRC packet
		for(loops=0; ;loops++)
		{
			printf("...downloading file: loop #%d..\n",loops);
			RTCDelayMicroSeconds(4000000L);  // wait for data
			nBytesRead=serBufReadTimed(tuport,dataBuf,4200,2000,true);
			printf("...nBytesRead = %d\n",nBytesRead); 
			if(nBytesRead == 0) goto NEXT_PROFILE; // should never be 0!
			
			// check the packet type
			result=CheckSIMDataPktHdr(dataBuf,&nBytesRead); // only check the 1st N bytes to save time
			//printf("...nBytesRead after cleanup = %d\n",nBytesRead);
			if(result == 1)
			{
				printf("...got CRC packet: ");
				//for(i=0; i< 8; i++) printf("0x%02x(%c),",dataBuf[i],dataBuf[i]);
				printf("\n");
				break; // should be the only way out of FOR loop
			}
			else if(result == 2)
			{
				printf("...got DATA packet: ");
				//for(i=0; i< 8; i++) printf("0x%02x(%c),",dataBuf[i],dataBuf[i]);
				printf("\n");
				
				bytesInPacket=(dataBuf[4]<<8)+dataBuf[5]-8;
				//printf("...# of data bytes= %d\n",bytesInPacket);
				CRCfromMMP=(dataBuf[6]<<8)+dataBuf[7];
				CRCcomputed=CRC16Block(&dataBuf[8],(ulong)bytesInPacket,0);
				//printf("...computed CRC= 0x%02x (%d)\n",CRCcomputed,CRCcomputed);
				
				RTCDelayMicroSeconds(3500000L);	 // wait for UIM to switch to receive mode	
				                                 // do not change the time!!	

// do not do CRC check to avoid repeated txmission if line noisy
#if 0
				// try only once more if CRC do not agree
				if(	(CRCmismatchCount == false) && (CRCcomputed != CRCfromMMP))
				{
					CRCmismatchCount=true;
					sprintf(tmpStr,"MMPtxfer(): CRC error.\n");
					printf("%s",tmpStr);
					logMsg(tmpStr);
					strcpy(cmdStr,"b01REQNAK\r");
				}
				else
#endif
				{
					strcpy(cmdStr,"b01REQACK\r");
					CRCmismatchCount=false;
					if(fp) 
					{
						if(fwrite(&dataBuf[8],1,bytesInPacket,fp) != bytesInPacket)
							fpWriteError=true;					
					}
				}
						
				result=IMMchat(tuport,true,cmdStr,cmdStr,1000);
				if(result == 0) goto NEXT_PROFILE; // something bad happened, quit.
				
			}
			else if(result == 3)
			{
				printf("...got EOD packet..\n");
				goto NEXT_PROFILE;  // sleep & wait for next profile
			}
			
		} // for(loops=0;
		
		if(fp) 
		{
			fclose(fp);
			// queue it only if file I/O OK
			if(fpWriteError==false) 
			{
				// zip MMP files, change the file extension to .gz.
				//  assume zipping time doesn't exceed 42 secs, the MMP timeout
				strcpy(fnameGz,fn);
				p=strchr(fnameGz,'.'); 
				sprintf(p+1,"gz");
				// gzip() returns 0 if success, else -1
				nowSecs=time(NULL);
				upldRet=gzip(fn,"rb",fnameGz,"wb",true );
				printf("file '%s' zipped in %ld secs\n",fnameGz,time(NULL)-nowSecs);
				if(upldRet < 0) 
				{
					printf("MMPtxfer(): gzip error, file will be txferred unzipped !\n");	
					strcpy(fnameGz,fn);
				}

				*filesInQueue=enQueue(fnameGz);
				printf(" --- file: %s queued (#%d)...\n",fnameGz,*filesInQueue);
			}
			else
			{
				printf(" --- file: %s I/O error, not queued...\n",fnameGz);
			}
			
		}
	} //while(1)..
		
NEXT_PROFILE: ;
	
	return result;

}	

