#ifndef _CONSTANT_H_
#define _CONSTANT_H_

#define CODENAME	"NEMO"
#define VERSION	"4.3"


// device 1
#define TPU_PWR_MODEM	1	// pin 22
#define TPU_toModem		2	// pin 23
#define TPU_fromModem	3	// pin 24

// device 2
#define TPU_PWR_IMM		4	// pin 25
#define TPU_toIMM		5	// pin 26
#define TPU_fromIMM		6	// pin 27

// device 3
#define TPU_PWR_ADCP	7
#define TPU_toADCP		8	// pin 29
#define TPU_fromADCP	9	// pin 30

// device 4
#define TPU_PWR_SBE37	10	// pin 31
#define TPU_toSBE37		11	// pin 32
#define TPU_fromSBE37	12	// pin 33

#define TPU_PWR_Dev5	13	// pin 34
#define TPU_toDev5		14	// pin 35
#define TPU_fromDev5	15	// pin 37 (36 is XCLK)

#define BAUD_IMM	9600
#define BAUD_ADCP	9600
#define BAUD_MODEM	115200
#define BAUD_SBE37	9600

#define MODEM_PWRUP_WAIT	105000000L
#define USERNAME	"nemo\r"
#define PASSWORD	"chabba\r"
#define IMM_PROMPT	"S>"

#define BASE_LOGIN_PROMPT	"name: "	// username, sometimes the prompt is truncated..
#define BASE_PASSWD_PROMPT	"Password: "
#define BASE_CMD_PROMPT	"IKSD>"
#define BASE_CMD_CD2DATADIR	"cd data\r"

#define QUIT_CH 0x11    // ctl-Q, exit character

#define CRLF	"\r\n"
#define CR	'\r'
#define LF	'\n'

#define FN_UPLOAD	"upload.bin" // file name for bin data to be uploaded
#define FN_CFG		"nemo.cfg"	// config file name
#define FN_REBOOTCOUNT	"reboot.txt"	// counter to keep track of how many times program reboots itself
#define FN_LOG		"nemo.log"	// log file name
#define FN_NEMO		"nemo.run"

#define FN_SUFFIX_ADCP	"adp"
#define FN_SUFFIX_SBE37	"sbe"

#define MAX_FILES_TO_UPLOAD	30

//#define MAX_REBOOTS_ALLOWED	30

#endif
