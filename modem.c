// Dunalp's modem.c modified for just dialing Iridium;
// for testing telnet negotiation..


#include	<cfxbios.h>		// Persistor BIOS and I/O Definitions
#include	<cfxpico.h>		// Persistor PicoDOS Definitions

#include	<ctype.h>
#include	<errno.h>
#include	<stdarg.h>
#include	<stddef.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>

#include "constant.h"
#include "prototype.h"


static char tmp[20];

#define LOGIN_ATTEMPTS	4

/*------------------------------------------------------------------- 
logging into remote host (ohm.apl.washington.edu).  
returns 1 if successful, 0 if not.
--------------------------------------------------------------------*/
int login(
TUPort *tuport)
{
int i,result=0;
	
	printf("\nStart login process...\n");
	// check to see if already logged in
	//result=serDeviceChatCF2(tuport,true,"\r", BASE_CMD_PROMPT,5000L,tmp,0);
	result=serDeviceChatCF2(tuport,true,"\r", BASE_CMD_PROMPT,3000L,tmp,0);
	if(result>0) 
	{
		printf("got command prompt...\n");
		return 1;
	}
	else
	{
		// send a CR to get a new login prompt,
		for(i=0; i< LOGIN_ATTEMPTS; i++)
		{
			printf("login attempt: %d...\n",i);
			result=serDeviceChatCF2(tuport,true,"\r", BASE_LOGIN_PROMPT,3000L,tmp,15);
			if(result>0 ) break;
		}
		if(i == LOGIN_ATTEMPTS) return 0;
		printf("got '...%s' prompt...\n",BASE_LOGIN_PROMPT);
			
		if(serDeviceChatCF2(tuport,true,USERNAME, BASE_PASSWD_PROMPT,5000L,tmp,0) < 0) return 0;
		printf("got '...%s' prompt...\n",BASE_PASSWD_PROMPT);
			
		if(serDeviceChatCF2(tuport,true,PASSWORD,BASE_CMD_PROMPT,5000L,tmp,0) < 0) return 0;
		printf("got '%s' prompt...\n",BASE_CMD_PROMPT);
		
		if(serDeviceChatCF2(tuport,true,BASE_CMD_CD2DATADIR,BASE_CMD_PROMPT,5000L,tmp,0) < 0) 
		{
			printf("logged in, but unable to cd to 'data' directory...\n");
		}
		else
			printf("in 'data' directory...\n");
		
		
		return 1;
	}

}


int logout(
TUPort *tuport)
{

	if(serDeviceChatCF2(tuport,true,"logout\r", BASE_LOGIN_PROMPT,10000L,tmp,0) < 0) return 0;
	printf("logged out...\n");
	return 1;
}



short fileTxfer(
TUPort *tuport,
char *fn)
{
long xfrd;
short ret=0;


	printf("\nUploading file '%s'...\n",fn);
// wavelet server is in kermit receive mode
#if 0	
	printf("Starting kermit txfer...\n");
	if(serDeviceChatCF2(tuport,true,"kermit -r\r", "RECEIVE...",10000L,tmp,0) < 0)
	{
		printf("kermit start error..\n");
		return 0;
	}
#endif
	
	// kermitTransfer() returns 0 if success
	if((ret=kermitTransfer(fn,strlen(fn), tuport, &xfrd)) == 0)
	{
		printf("file '%s' txferred, bytes= %ld.\n",fn,xfrd);
		ret = 1;
	}
	else ret = 0;
	
	return ret;
}


short upload(
TUPort *tuport,
char *fn)
{
int upldRet;

	upldRet=login(tuport);
	
	if(upldRet == 0) 
	{
		printf("upload(): Unable to login...\n");
		return 0;
	}
	else upldRet=fileTxfer(tuport,fn);
	
	if(upldRet == 0)
	{
		printf("upload(): Unable to txfer file...\n");
		return 0;
	}

	return 1;
}

