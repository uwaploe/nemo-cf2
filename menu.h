/*
**$Id: menu.h%v 1.1 1994/10/29 01:34:35 mike Exp mike $
*/
#ifndef _MENU_H_
#define _MENU_H_

typedef struct{int index;char *label;} menuItem;

typedef void (*MenuCallback)(int);

typedef struct {
	const char	*label;
	MenuCallback	f;
        int         call_data; 
} MenuEntry;


typedef struct {  // temp array to hold params label & default values
        unsigned int    ind;
        char      *label;
        float     value;             
} paramSetup;


int show_menu(const char *title, register MenuEntry *menu, size_t n,const char *from);

#endif

