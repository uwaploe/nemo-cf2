				NEMO Sub-Surface (NEMO-SS) User Manual
				         Firmware Ver 4.2
						   
						   Feb 11, 2013


1. Communication equipments:
	
	Only a simple terminal emulator is needed to communicate with the NEMO-SS. 
	Connection between the PC to the CF2 is via a RS-232 serial, or a USB-serial 
	cable if the laptop doesn't have a built-in serial port.  The terminal 
	emulator settings should be 9600, N, 8, 1, and uses only CR as EOL character.
	

2. NEMO-SS sensors and devices:

	- MMP: 9600 baud
		self-powered, needs to be programmed separately via a separate COM port.
	- SBE37: 9600 baud
		self-powered, programmed through NEMO-SS, or separately since it will run 
		autonomously and log internally.
	- ADCP: 9600 baud
		self-powered, programmed through NEMO-SS, or separately since it will run 
		autonomously and log internally.
	- Cell modem: 115,200 baud
		power controlled by NEMO-SS.
		
		
3. NEMO-SS Firmware:

	NEMO-SS firmware "nemo.run" is stored on the CF2's external compact flash.  
	It is loaded into and runs from the CF2 RAM when invoked.
	
	At power up, NEMO-SS is invoked by "autoexec.bat" and starts a 5-second countdown.  
	(If "autoexec.bat" file doesn't exist, the operator will see a "C:\>" prompt.  
	Type "nemo" to invoke the program and the countdown will start.  "nemo" will
	automatically generate a copy of 'autoexec.bat".)
	If no key is pressed during the countdown, NEMO-SS goes into deployment mode.  
	This feature is designed for NEMO-SS to self-restart in case of a crash during 
	the deployment. 
	To check parameters prior to deployment or do bench-testing, operator should 
	press a key to get into the main menu.


NOTE: If the deployment has been started and need to abort or get into picoDOS, power 
	must be cycled, or CTL-Q entered, during a very short period (~1 sec) right after
	the CF2 just wakes up from low-power sleep.  An alternative is to disconnect the 
	power for a sufficient period to allow the CF2 to wake up from low-power sleep 
	and then quit in the absence of power.

	
	To load firmware onto the external compact flash, use "Motocross" terminal 
	application.  Press F7 key and the operator will be presented with a file 
	dialog.  Browse to the directory containing "nemo.run" (..\..\nemo\bin) and
	click OK.  Type 's nemo.run' after 'g' is displayed (gs nemo.run) to save it 
	onto the CF flash.  Will overwrite the old copy.  Sometimes 'g' may not show up,
	but just go ahead and do the same.
	

4. NEMO-SS Hardware:
	
	The CF2 controller requires a 3V lithium button battery (CR2032) to maintain the real-time
	clock while in power suspend mode.  The battery should be replaced whenever necessary.
	
	
5. Main menu:

	Note: Set the terminal emulator to use CR as the EOL character.
	
	-------------------------------------------------------------------
		 NEMO, Ver 4.2,  Main Menu, Build: Feb 5 2013, xx:xx:xx
	-------------------------------------------------------------------
	 1)  Exit to picoDOS
	 2)  Check available memory
	 3)  Test CF2 power suspend
	 4)  Set CF2 clock
	 5)  Enter interval to upload SBE37/ADCP data [15 min]
	 6)  Enter interval to upload via cell modem [1 hrs]
	 7)  Enter minutes past hour to upload data via cell modem [5 min]
	 8)  Enter IMM/MMP sub-menu
	 9)  Enter cell modem sub-menu
	10)  Enter ADCP sub-menu
	11)  Enter SBE37 sub-menu
	12)  Start deployment!
	Enter selection (1-11): 
	 
	 
	 Item 1: exit to picoDOS for system level operations such as checking/deleting 
			 data files, reloading the firmware, setting date, etc.
	 Item 2: check the amount of RAM avialable for program execution after the code 
			 is loaded.
	 Item 3: Check/test CF2 low-power sleep.
	 Item 4: Check/set CF2 clock.  (Clocks for sensors need to be set separately.)
	 Item 5: Set the interval for NEMO-SS to wake up and query the SBE37 and the ADCP.
	 Item 6: Set the interval for NEMO-SS to wake up and upload data via cell modem.
	 Item 7: Set the time after the hour to upload data via cell modem
	 Item 8: Direct comm with IMM (Can't talk to MMP unless the latter is in UIM/SIM mode
			 and only for testing file transfers.)
	 Item 9: cell modem power control & direct comm with Wavelet server 
	 Item 10: Direct comm with ADCP
	 Item 11: Direct comm with SBE37
	 
	 
	 To talk to a device/sensor such as SBE37 or ADCP, select the respective menu
	 item and the "Direct comm with .." option.  Example for SBE37 is shown 
	 below:
	 
		-----------------------------
				   SBE37 Menu
		-----------------------------
		  1)  Direct comm with SBE37
		  2)  Return to main menu
		 Enter selection (1-2): 
	 
	For convenience, some commands pertaining to the selected device are provided in 
	direct comm mode so the operator doesn't have to refer to the device's user manual.
	
	
6. Deployment:

	- MMP:
		- check/test attached sensors.	
		- "file deletion" feature must be enabled.  MMP retains only data from the last 
		  60 profiles.		  
		- deploy:
		
			 Start     Z| Countdown delay          =     00:00:10 [HH:MM:SS]
			 
			 Schedule  I| Pair start interval      = 000 02:00:00 [DDD HH:MM:SS]
					   R| Reference date/time      =   02/20/2009 19:00:00
					   B| Burst Interval           =     Disabled
					   N| Pairs per burst          =     Disabled
					   P| Paired profiles                 Enabled
					   F| Profiles / file set      =            1
	 
				  
	- deploy NEMO-SS:
		- power up
		- if countdown in progress:
			- hit any key to get into main menu
			- select item 1 to get into picoDOS.
		- at 'C:\>' prompt, type 'date mm/dd/yyyy hh:mm:ss'.  CF2 will remember this date as 
		  long as the power holds.  (Also make sure the CR-2032 backup battery is fresh).
		- check CF flash, if needed:
			- type 'dir'.
			- if old data exist, type 'del *.dat' to delete.  (Or 'format' to quicky erase everything.
			  But will need to reload 'nemo.run'.  See section 3.)
		- type 'nemo' to start the program.  Hit any key to get into main menu.
		- check date/time (item 4).  Enter correct date/time if not current. (Note: Terminal emulator
		  should use only CR as EOL character.)
		- test SBE37 
		- test ADCP
		- test modem
		- check/set query & upload intervals
		- select item 12 to start deployment.
	

7. Algorithm:

	In deployment, NEMO-SS (CF2 controller) will go into low-power sleep mode.  It wakes
	up under 3 conditions:
	a. at programmed times (default: every 15 minutes) to query SBE37 & ADCP. 
	b. at an arbitrary time MMP sends a wake-up tone at the end of a profile.  
	c. at programmed interval (hrs) and time past the hour (default: 5 min) to upload data via 
	   cell modem.
	
	In (a), NEMO-SS will query the SBE37 and the ADCP for the latest sample and put the data files
	in an upload queue (last-in first-out). In (b), NEMO-SS downloads files from the MMP using a 
	special file-transfer protocol (Appendix J, MMP manual), and puts the files in the upload queue.  
	In (c) NEMO-SS powers up the cell modem and uploads up to 30 data files in the queue to the server.  
	
	If an MMP download session goes past a 15-minute mark, then that SBE+ADCP query session will 
	be skipped.  Same thing applies to an MMP upload session.  Conversely, if MMP sends a wake-up tone 
	while NEMO-SS is in (a) or (c), the tone will be ignored and the transfer session skipped, and
	the files will be uploaded at the next opportunity.  If a modem upload session is blocked by a MMP 
	txfer session, or anything else, the upload will take place an hour later.  After that it goes 
	back to the programmed interval.
	
	To reduce the time and power to upload data files, NEMO-SS compresses (gzip) the larger binary 
	files from the MMP.  Smaller ASCII files from the SBE37 and ADCP are not compressed.  File 
	transfer from NEMO-SS to the server is done with Kermit protocol.  NEMO-SS will attempt to 
	upload up to 30 files in the queue at the scheduled upload time.  If for any reason the transfer 
	process is interrupted or fails, the session is terminated and that file is deleted from the queue.
	This is to prevent endless attempts in uploading a possibly corrupted file.  In any event, remaining 
	files in the queue will be uploaded at the next scheduled upload time.  Files are removed from the 
	queues after upload, but not deleted from the CF2's compact flash, except the compressed files.  
	In other words, all the raw SBE37, ADCP, and MMP data files are saved.
	
	
8. Data files on NEMO-SS:

	MMP: 
		- "file deletion" feature must be enabled.  MMP retains only data from the last 
		  60 profiles.
	
	NEMO-SS:
		- Data from SBE37 are stored in files with names of the form "yjjjhhmm.sbe", 
		  where j is the last digit of the year & jjj is the Julian day.  A typical 
		  SBE37 data file looks like the following:
		
			19.9924,  0.00006,    0.021,   0.0099, 07 Feb 2011, 18:30:11

			For OutputFormat=1 (default): converted decimal data:
			tttt.tttt,ccc.ccccc,ppppp.ppp,ssss.ssss,vvvvv.vvv, dd mmm yyyy, hh:mm:ss
			where tttt.tttt = temperature (�C, ITS-90). ccc.ccccc = conductivity (S/m). ppppp.ppp = pressure (decibars);  
			ssss.ssss= salinity (psu);  dd mmm  yyyy = day, month, year. hh:mm:ss = hour, minute, second.
			Leading zeros are suppressed, except for one zero to the left of the decimal point. All data 
			is separated with a comma; date and time are also preceded by a space.

		- Data from ADCP are stored in files with names of the form "yjjjhhmm.adp", 
		  where j is the last digit of the year & jjj is the Julian day.  A typical 
		  ADCP data file looks like the following:
		  
			7F7FB402000612004D008E006801D601440200003226CB4100
			35041B0A006400B00001400500D0070001001F000000007D3D
			2B017900010532001900C4000000E71699090000FF00DE1A00
			...
			...
			64000000640000006400000064000000640000006400000064
			00000064000000640000006400000064000000640000006400
			00006400000064000000640000006400000064000000640000
			006400000064000000640000006400EB394675>		  
			
			
		  For data format, refer to section 5 (p. 122) of "WorkHorse Commands and Output Data Format_Nov07.pdf"
		  document.
			
		- Data downloaded from the MMP retain the file names and the original 
		  format.
		  
		  a0000xxx.dat - ACM data
		  c0000xxx.dat - CTD data
		  s0000xxx.dat - SUNA data
		  e0000xxx.dat - engineering data
		  
		  These are zipped first, uploaded and then un-zipped by the server.  
		  
		- all the data files are saved on the compact flash.
		

9. Real-time data:
		
	Data uploaded from NEMO-SS can be found at http://wavelet.apl.washington.edu/~mooring/NEMO_Subsurface/.
	
	
10. Misc notes on hardware:

	- IMM:
		- Hardwired to be powered ON all the time.
		  IMM needs to be on all the time to detect wake-up tones from the MMP.
		  Therefore its power cannot be controlled by the CF2 since CF2 shuts everything down 
		  when it goes to sleep in power suspend mode. 
		- IMflag (pin4) wired to pin 38 of CF2
		- alternates between configType=1 to talk to MMP and configTYpe=2 to detect wakeup tone.		
		- getcd:
			<ConfigurationData DeviceType='SBE90554 IMM' SerialNumber='70000957'>
			<Settings ConfigType='2'
			DebugLevel='1'
			BaudRate='9600'
			HostID='Host ID not set'
			GdataStr='GDATA'
			HostPrompt='x'
			ModemPrompt='IMM>'
			DeviceID='0'
			EnableHostFlagWakeup='0'
			EnableHostFlagConfirm='0'
			EnableHostFlagTerm='0'
			EnableSerialIMMWakeup='1'
			EnableHostPromptConfirm='1'
			EnableHostServeOnPwrup='0'
			EnableAutoIMFlag='1'    (uses IMflag to wake up the host)
			EnablePrompt='1'
			EnableHostWakeupCR='1'
			EnableHostWakeupBreak='0'
			EnableEcho='1'
			EnableSignalDetector='1'
			EnableToneDetect='1'
			EnableFullPwrTX='0'
			EnableBackSpace='1'
			EnableGDataToSample='0'
			EnableStripHostEcho='0'
			EnableBinaryData='1'
			SerialType='1'
			TermToHost='254'
			TermFromHost='254'
			SerialBreakLen='5'
			MaxNumSamples='40'
			GroupNumber='0'
			THOST0='0'
			THOST1='5'
			THOST2='1000'
			THOST3='12000'
			THOST4='500'
			THOST5='5'
			TMODEM2='500'
			TMODEM3='18000'
			TMODEM4='100'	
	
	- cell modem:
		- power supplied and controlled by NEMO-SS

		
	- SBE37:
		- self-powered (external option yet?)
		- must be set to S> prompt mode
  		- Commands:
          	datetime=mmddyyyyhhmmss
			Initlogging
			SampleInterval=120;
			TxRealtime=N;
			OutputFormat=1
			OutputSal=Y
			OutputSV=N
			StartNow
			DS  ("display settings" should give:)

				SBE37SM-RS232 3.0h SERIAL NO. 7525 17 Oct 2010 20:25:37
				vMain = 6.99, vLith = 3.27
				samplenumber = 0, free = 492158
				logging
				sample interval = 120 seconds
				data format = converted engineering
				output salinity
				transmit real-time = no
				sync mode = no
				pump installed = yes, minimum conductivity frequency = 3236.5

		- Test the output with the "SL" command.
		- Then "QS" to put into quiescent mode.

		
	- Workhorse ADCP:
		- self-powered
		- uses a serial BREAK signal to interrupt acquisition.  Unfortunately when CF2 goes to 
		  sleep all the IO lines go low and ADCP thinks it has received a BREAK signal.  The
		  solution was to add a voltage regulator to permanently power the transceiver between 
		  the CF2 and the ADCP.
		- Set-up: (lower-case OK)  ---note the settings specific to the ADCP sampling may 
		  be altered some prior to deployment (e.g. WF,WP,WN,WS,TE,TP) to optimize battery usage.

			CR1
			CF11001
			CH1
			EA0
			EB0
			ED150
			ES35
			EX11111
			EZ1111101
			RI0
			WA50
			WB0
			WD111100000
			WF88
			WN45
			WP13
			WS50
			WV175
			TE00:02:00.00 (sample interval)
			TP00:01.00
			AZ
			CK
			CS (starts pinging)

		- TO query a parameter, put a ? after the command:
			CF?
			WD?
					
		- sample WorkHorse output:

			- send 'BREAK' signal first to interrupt sample mode.
			  takes ~3 seconds before getting the prompt (>).
			- send 'ce' (get last ensemble). takes ~1.5 seconds to download 
			  the data.
			  
			>ce 
			7F7FB402000612004D008E006801D601440200003226CB4100
			35041B0A006400B00001400500D0070001001F000000007D3D
			2B017900010532001900C4000000E71699090000FF00DE1A00
			0014800001000B0104003B1D43000000EE050000F56399FFFE
			FF23005F0700000000010067AF5B494759829F0001008821E4
			7F0000001F00000000140B0104003B1D430001008000800080
			00800080008000800080008000800080008000800080008000
			80008000800080008000800080008000800080008000800080
			00800080008000800080008000800080008000800080008000
			80008000800080008000800080008000800080008000800080
			00800080008000800080008000800080008000800080008000
			80008000800080008000800080008000800080008000800080
			00800080008000800080008000800080008000800080008000
			80008000800080008000800080008000800080008000800080
			0080008000800080008000020A0B0B0A0D0B0C0C090A0A0A0C
			0D0B090A0B0E0C090C090A0B0B0A080D0D0A0D0A09090C0A0C
			090C0A0B0B0B0D0D0D0D0B0E0C0B0B0D0A090C0B0B0B0A090B
			0A0C0A0A0C0B090B070B0C0A0C0D0C0A0E0C0C0A0C0B0B0C08
			0A0A0B090F0E0B0B0B0C0B0B09080B0E0C0C090B0003272A2E
			28292C302B292C312B292C302A292C302A292C302B292C302B
			2A2C302B292C302A292C302B292C302B292C302B292C302B29
			2C302A292C302B2A2C312B2A2C312B2A2C302B2A2C302B292C
			302B2A2C302A292C302B292C302A2A2C302A2A2C302B292C30
			2B2A2C302B0004000064000000640000006400000064000000
			64000000640000006400000064000000640000006400000064
			00000064000000640000006400000064000000640000006400
			00006400000064000000640000006400000064000000640000
			006400000064000000640000006400EB394675>

			- to resume sample mode, send 'cs'.
		
	
	- MMP:

		- SBE52 CTD: need to configure it per "SeaBird_CTD_Prep.pdf" instructions.
		
		- need to enable "file delete" feature, otherwise MMP's response delay to 
		  "#01REQNEW" will take longer and longer with more and more profiles done.
		  
		- UIM listening loop: supposed to have 3 attempts, each time the MMP sends 
		  out a wake-up tone.  But only 2 are sent!?  And after sending the 2nd tone 
		  the MMP goes right back to profiling!
			  
		- test setup for a pair every 2 hours:

			ID        M| Mooring ID               = 002

			Start     Z| Countdown delay          =     00:00:10 [HH:MM:SS]

			Schedule  I| Pair start interval      = 000 02:00:00 [DDD HH:MM:SS]                
					  R| Reference date/time      =   02/20/2009 19:00:00
					  B| Burst Interval           =     Disabled                
					  N| Pairs per burst          =     Disabled
					  P| Paired profiles                 Enabled                
					  F| Profiles / file set      =            1              

			Stops     S| Shallow pressure         =          0.0 [dbar]                
					  D| Deep pressure            =         20.0 [dbar]                
					  H| Shallow error            =         10.0 [dbar]                
					  E| Deep error               =         10.0 [dbar]                
					  T| Profile time limit       =     00:16:11 [HH:MM:SS]                
					  C| Stop check interval      =            2 [sec]
					  L| Fluorometer              =     Disabled
					  O| OBS Turbidity            =      Enabled  		        
									 Gain         =          100   		        
									 Sample/avg   =            1

			Endurance  | Power for single profile =          9.9 [mAh]  
					   | Total profiles/(240 Ah)  =        23010  
					   | Est. battery expiration  =   09/20/2013 09:52:39    

			Deploy    V| Verify and Proceed		


	-DATA ACCESS
		

        http://wavelet.apl.washington.edu/~mooring/NEMO_Subsurface/	
			
	- wavelet.apl.washington.edu:   (MORE COMING HERE?)
		
		Typical txfer session: (note dropped leading characters in some of the lines)

		Internet Kermit Service ready at Tue Jan 18 17:03:09 2011

		C-Kermit 8.0.211, 10 Apr 2004

		wavelet.apl.washington.edu


		me: nemo  (should be 'User name:', leading characters were dropped)

		er nemo's Password: 

		User nemo logged in.

		C-Kermit 8.0.211, 10 Apr 2004, for Linux


		Copyright (C) 1985, 2004,

		  Trustees of Columbia University in the City of New York.

		Type ? or HELP for help.


		home/nemo/) IKSD>
			
		...
		...
		
		
	 - Feb 2013: NEMO_SS recovered.  The 3V backup battery (CR2032) in NEMO_SS controller was dead, 
	   causing weird date/time outputs.  Battery was replaced.
	   

	   
	 
