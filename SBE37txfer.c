#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<stddef.h>
#include	<time.h>
#include <ctype.h>	// for toupper() function

#include "prototype.h"
#include "constant.h"



int SBE37txfer(
TUPort *tuport,
int *filesInQueue)
{
short i,result = 0;		// no errors so far
uchar cmdStr[80],dataBuf[300],cTemp[60],fn[16];
short nBytesRead;
FILE *fp;
time_t nowSecs;

	// open file first
	nowSecs=time(NULL);
	strftime(cmdStr, sizeof(cmdStr)-1, "%y%j%H%M", localtime(&nowSecs));
	sprintf(fn,"%s.%s",&cmdStr[1],FN_SUFFIX_SBE37);  // keep only the last digit of year


	result=serDeviceChatCF2(tuport,true,"\r", ">",2000L,cTemp,0);
	result=serDeviceChatCF2(tuport,true,"\r", ">",2000L,cTemp,0);
	if(result < 1)
	{
		sprintf(cmdStr,"SBE37txfer(): No comm with SBE37!\n");
		printf("%s",cmdStr);
		logMsg(cmdStr);
		return 0;		
	} 
	
	sprintf(cmdStr,"sl\r"); // download 
	TUTxPutBlock(tuport,cmdStr,strlen(cmdStr),1000);
	// get command echo & metadata
	nBytesRead=serBufReadTillExpectedStringTimed(tuport,dataBuf,"S>",3000,false);
	if(nBytesRead == 0) 
	{
		sprintf(cmdStr,"SBE37txfer(): No bytes downloaded from SBE37.\n");
		printf("%s",cmdStr);
		logMsg(cmdStr);
		return 0;		
	} 
	
	printf("SBE37txfer(): file name: %s, bytes=%d\n",fn,nBytesRead);
	if((fp=fopen(fn,"wb")) == NULL)
	{
		sprintf(cmdStr,"SBE37txfer(): Fopen(wb) error!\n");
		printf("%s",cmdStr);
		logMsg(cmdStr);
		return 0;		
	} 
	
	if(nBytesRead > 0) 
		// do not write 'sl\r\n' at the beginning & 'S>' at the end
		fwrite(&dataBuf[4],1,nBytesRead-6,fp);
	else
		fwrite(&dataBuf[4],1,(-nBytesRead-4),fp);
	fclose(fp);
	
	*filesInQueue=enQueue(fn);
	
	sprintf(cmdStr,"qs\r"); // resume logging,
	TUTxPutBlock(tuport,cmdStr,strlen(cmdStr),1000);
	return 1;
}
