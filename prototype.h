
#ifndef _PROTOTYP_H_
#define _PROTOTYP_H_

#include	<cfxbios.h>		// Persistor BIOS and I/O Definitions
#include	<cfxpico.h>		// Persistor PicoDOS Definitions
#include "menu.h"
#include "time.h"


void menu_main(char*,char *);
void menu_imm(void);
void menu_adcp(void);
void menu_modem(void);
void menu_SBE37(void);
void serialComm(short );
void displayMenu( char *title, MenuEntry *menu, size_t n,const char *from);
short serDevIOCF2(TUPort *port,bool);
void consoleInput(char *);

int serPortInit(TUChParams*);
short serGetLastLineTimed(short port,char *str,short ,long);
short serGetLineTimed(short port,char *str,short,long timeout);
long serGetLineMultiRead(short port,char *str,short);
short serGetByteTimed(short port , unsigned short*, long);
long serBufReadTimed(TUPort *port,unsigned char *str,long len,long ,bool);
long serBufReadTillExpectedStringTimed(TUPort *port,uchar *str,uchar *expect,
	long timeout,bool waitForEachByte); // 0=return after timeout, 
long serBufRead(short port,unsigned char *str,long len);

short n_substr(
char *del,  // array of delimiters
int     m_substr,       /* Maximum number of subsrtings (Input)*/ 
char    *str,           /* String to convert (Input&Output)*/
char    *substr[]);      /* Pointers to begining of substrings (Output)*/


short IMMcaptureLine(short);
void IMMreset(short);

short serDeviceChatCF2(
TUPort *port,
bool,
char *send,     // string to write to the modem.
char *expect,   // response string to wait for, must be 0 or >= 2 and < 31 chs.
long timeout,
char *response,     // response string of length len to be returned, ending with the expected string  
int);  // maximum number of ms to wait for each ch.


short serDeviceChatGetLine(
TUPort *port,
char *send,     // string to write to the modem.
char *expect,   // response string to wait for, must be 0 or >= 2 and < 31 chs.
char *response,     // line-terminated response string 
short,  // line terminator
long timeout);
int serPortGetLine(TUPort *,uchar *,int);

void quit(void);

time_t dateTimeStrToSeconds(char *,char *,char *,char *);

void logMsg(char *);

void togglePower(short);
void readIOpin(short);
void powerOff(short pin);
void powerOn(short pin);
void setTPUPin(short pin);


long freemem(void);

void IMMcomm(void);
void ADCPcomm(void);
void SBE37comm(void);
void ADCPinit(void);
void MODEMcomm(void);
//void autoInitModem(void);
//void autoDialModem(short);
void autoLogin(void);
int testFileTxfer(void);


int CfgFileRead(void);
void CfgFileWrite(void);
int menuCfg(void);

void setClock(void);
char *timeStamp(void);

void setSBEUploadInterval(void);
void setModemUploadTime(void);
void setModemUploadInterval(void);

void setMooringID(void);
//int cvtGPS2Packet(char *str,struct GPSpacket *gps);

void configFileRead(char*);
void configFileWrite(char*);
void setStartDateTime(void);

void testPwrSuspend(void);
int login(TUPort *);
int logout(TUPort *);

int gzip(char *,char *,char *,char *,bool );
short fileTxfer(TUPort *,char *);
short upload(TUPort *,char *);
int kermitTransfer(char *, int , TUPort *, long *);

void autoExec(int);
short rebootCountRW(char *fname,bool reset);
void resetReboot(void);
void exitMenu(void);

void TUBreak(TUPort *tup, uchar ms);
void ADCPbreakSignal(void);

int MMPtxfer(TUPort *, int*);
int IMMchat(TUPort *port,bool ,char *send, char *expect,long timeout);

void deQueue(int *nf,char *fn);
int enQueue(char *fname);
void initQueue(void);
void deQueue2(int *nf,char *fn);
int enQueue2(char *fname);
void initQueue2(void);

int IMMsetConfigType2(TUPort *);
int IMMsetConfigType1(TUPort *);

int ADCPtxfer(TUPort *, int*);
int SBE37txfer(TUPort *, int*);
#endif
