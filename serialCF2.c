#include        <stdio.h>
#include        <stdlib.h>
#include        <string.h>
#include        <ctype.h>
#include        <math.h>
#include        <time.h>
//#include      "_cfx_console.h"
#include	<cfxbios.h>		// Persistor CF1 System and I/O Definitions
#include	<cfxpico.h>		// Persistor CF1 PicoDOS Definitions  

#include "constant.h"
#include "prototype.h"

extern TUPort *tuport_imm,*tuport_ADCP,*tuport_modem,*tuport_SBE37;

void TUBreak(TUPort *tup, uchar ms);

short n_substr(char *, int, char *,  char *substr[]);  
/*---------------------------------------------------------------------------
n_substr() - parse a string using specified delimiter(s) into substrings
ex: 122033,sdf,333,5454;  ,,,234,332,65330.
---------------------------------------------------------------------------*/
short n_substr(           /* Convert string to substrings */
								/* Returns number of substrings */
char    *del,           /* Set of deliminitors (Input) */
int     m_substr,       /* Maximum number of subsrtings (Input)*/
char    *str,           /* String to convert (Input&Output)*/
char    *substr[])               /* Pointers to begining of substrings (Output)*/
{
		int n;  /* number of characters in str */
		int m;  /* number of deliminators */
		int sub;        /* index of current substr */
	int i;  /* index of character in str */
	int j;  /* index of deliminator */

	n= strlen(str);
	m= strlen(del);
	sub= 0;

	/* Convert deliminators to NULL s */
	for( i=0; i<n; i++ )
		for(j=0; j<m; j++ )
			if( str[i]==del[j] ) str[i]='\0';

	/* Determine the pointers to the begining of each substring   */
	if( str[0]!='\0' ) substr[sub++]= str;  /* First substring  pointer */


#if 0
			// counting consecutive delimiters, ie., xxx,,,bb will be parsed as xxx, \0, \0, bb
		 for( i=1; i<n; i++ )
			if(  str[i]=='\0'  && sub<m_substr ) // count even if consecutive delimeters
					substr[sub++]= str+i+1;
#endif


//#if 0
		/* not counting consecutive delimiters */
		for( i=1; i<n; i++ )
		if( str[i-1]=='\0' && str[i]!='\0'  && sub<m_substr ) 
								substr[sub++]= str+i;
//#endif


		  return(sub);
}




short serDevIOCF2(TUPort *port,bool);
/*--------------------------------------------------------------------------
serDevIOCF2(Port): 
	reads input from a serial port and keyboard.
	characters typed in at keyboard are sent out the serial port.
	bytes from serial port is returned in *str.

	returns 1 if: 1. 'serDevIO()' returns 1 ('Ctrl Q' abort),
		    0     2. a CR is received at serial inport.
----------------------------------------------------------------------------*/
short serDevIOCF2(
TUPort *port,
bool sendLFafterCR)
{
ushort c;
int ret=0;

	fflush(stdin);
	printf("\n\nDirect comm with serial device at port 0x%0x (CTL-Q to exit)... \n",port);

	while (1) 
	{
		// outgoing.  monitor keyboard inputs for commands to devices as well as 'Ctrl q' to exit
		if( CIOgetq()) 
		{//if-1
			c=CIOgetc();

		    if(c == 0x11) return(1);
	    	else if(c == 0x02) // ctl-b, only for RDI Workhosre ADCP to send a BREAK signal !!!!!!
	    	{
				TUBreak(tuport_ADCP,70);		
			}
		    else if(c != 0x11) 
		    {
				//printf("%x,%c",c,c); fflush(stdout); // echo typed ch
		     	TUTxPutByte(port,c,true);
				// sometimes send \r and \n programmatically is too fast for an external device.
				// need to put a delay in between!
				if(c == '\r' && sendLFafterCR) {RTCDelayMicroSeconds(1000L); TUTxPutByte(port,'\n',true); }
			}
	    } // if-1

	    // incoming...
	    if ( TURxQueuedCount(port)>0) 
	    {
	    	c=TURxGetByte(port,false);
			printf("%c",c); fflush(stdout);
		}
	} // while(1)...      
	                            
    return(ret);
 }  // -----serDevIO()------------------



void consoleInput(char *str);
void consoleInput(
char *str)
{
unsigned short c;
int ret=0,n=0;

	while (1) 
	{
		// kick watchdog here if needed
		if( CIOgetq()) 
		{
			str[n++]=c=CIOgetc();

			printf("%c",c); fflush(stdout); // echo typed ch
			if(c == '\r') {str[n]=0; break;}
	    } // if-1

	} // while(1)...      
	                            
 }  // -----consoleInput()------------------


#if 0
/*---------------------------------------------------------------------------
serGetLastLineTimed(): read the whole buffer and extract the bytes bracketed 
by the last 2 terminator's. If the last byte is not a terminator, will wait 
for timeout ms to read it.  calling function must reserve enough space for the 
string.  If there is only 1 terminator in buffer, *str points to the first byte.
if none found, *str returned with a null char.
function also returns -1 for no terminator, 1 for found.
----------------------------------------------------------------------------*/
short serGetLastLineTimed(
short port,       
char *str, // calling function should allocate same size to 'str' as the serial input buffer
short termChar,
long timeout) // ms
{
int i,n2,nCR=0,n=0;
char *temp;
ushort numBytes;

	numBytes=U4SRxCharsAvail(port);
	//printf("  serGetLastLineTimed(): port: %d, %d bytes\n",port,numBytes);
	
	
	WATCHDOG_STROKE;
	temp = str;
	while(U4SRxCharsAvail(port) ) { // read the whole buffer
		 temp[n++]=U4SRxGetChar(port);
	}
	temp[n] = '\0';
	
	if(temp[n-1] != termChar) serGetLineTimed(port,&temp[n],termChar,timeout);
	//printf("bytes = %d\n%s\n",n,temp);

	n=strlen(temp);
	for(i=n-1; i>=0; i--) {//for-1
		 if(temp[i] == termChar) {
			nCR++;
			if(nCR == 1 ) {n2 = i+1; temp[n2]='\0';}
			if(nCR == 2) {
				strcpy(str,&temp[i]);
			 	return 1;
			}
		}
	}//for-1

	if(nCR == 0)
		 {str[0]='\0'; return -1;}  // no CR found
	else
		 {str=temp; return 1;} // only 1 CR found

}
#endif


/*--------------------------------------------------------------------
serBufReadTimed():
- 2 options for time out:
   1: returns after total of timeout ms, 
   2: keep reading until no byte in timeout ms.
- data string is null-terminated.
--------------------------------------------------------------------*/
long serBufReadTimed(
TUPort *port,
uchar *str,
long len,	// max # of total bytes to read
long timeout, //ms
bool waitForEachByte) // 0=return after timeout, 
				  // 1=keep reading until no byte in timeout ms, ie, timer resets for each byte
{
ushort c;
long temp=timeout*1000L,n=0,nbytes;
RTCTimer tt;
short i;

	RTCElapsedTimerSetup(&tt);
	while (RTCElapsedTime(&tt)  < temp) 
	{
		nbytes= TURxQueuedCount(port);  // p.72
		for(i=0; i<nbytes; i++) 
		{
			c=TURxGetByte(port,false);
			str[n++]=c;
		//printf("%02x,",c ); fflush(stdout);
		//if(c != 0x0a && c != 0x0d) printf("%c",c );
		//else printf(",%02x",c );
			if(n >= len) goto QUIT;
			if(waitForEachByte) RTCElapsedTimerSetup(&tt);
		}
	}

QUIT:
	str[n]=0;
	return n;
} // ----serBufReadTimed() -------



/*--------------------------------------------------------------------
serBufReadTillExpectedStringTimed():
read serial buffer till desired string is encountered.
- 2 options for time out:
   1: returns after total of timeout ms, 
   2: keep reading until no byte in timeout ms.
- data string is null-terminated.
- returns # of bytes if expected string found, else
  negative of # of bytes read. 
--------------------------------------------------------------------*/
long serBufReadTillExpectedStringTimed(
TUPort *port,
uchar *str,
uchar *expect,
long timeout, //ms
bool waitForEachByte) // 0=return after timeout, 
				  // 1=keep reading until no byte in timeout ms, ie, timer resets for each byte
{
ushort c;
long temp=timeout*1000L,n=0,nbytes,lenExpect;
RTCTimer tt;
short i;

	lenExpect=strlen(expect);
	RTCElapsedTimerSetup(&tt);
	while (RTCElapsedTime(&tt)  < temp) 
	{
		nbytes= TURxQueuedCount(port);  // p.72
		for(i=0; i<nbytes; i++) 
		{
			c=TURxGetByte(port,false);
			str[n++]=c;
			str[n]=0; // null-terminate
			if(n >= lenExpect)
			{
				if(!strncmp(&str[n-lenExpect],expect,(size_t)lenExpect)) return n;
			}
			if(waitForEachByte) RTCElapsedTimerSetup(&tt);
		}
	}

QUIT:
	str[n]=0;
	return (-n);
} // ----serBufReadTillExpectedStringTimed() -------




#if 0
//--------------------------------------------------------------------
long serBufRead(
short port,
uchar *str,
long len)	// use 0 if want to read till timeout
{
int c;
long n=0;

printf("... in serBufRead()\n");
	while (1) {
		if (U4SRxCharsAvail(port))	{
			str[n++]=c=U4SRxGetChar(port);
			// if(c != 0x0a && c != 0x0d) printf("%c",c );
			// else printf(",%02x",c );
			if(n == len)  break;
		}
		else break;
	}

	str[n]=0;
	return n;
} // ----serBufRead() -------


#endif



short serDeviceChatCF2(TUPort *,bool,char *,char *,long,char *,int); 

/*-------------------------------------------------------------------------
 CF2: Automate a "conversational" interaction with a serial device
 - should use the last few chars of an expected response, otherwise the host
	 may still be txmitting while the next serDeviceChat() is sending a command again.
 - calling routine is responsible for having enough space in the string!!!

returns 1, if successful,
		  -1, exceed timeout, no expected string found in response...
---------------------------------------------------------------------------*/
short serDeviceChatCF2(
TUPort *port,
bool clearBufferFirst,
char *send,     // string to write to the modem.
char *expect,   // response string to wait for, must be 0 or >= 2 and < 31 chs.
long timeout,  // maximum number of ms to wait for 'expect' string
char *response,     // first len bytes in the buffer to be returned,   
int len)	// len=0, don't return anything
 
{
register char	*p, *q;
register int	i, n;
char response_buf[128];
ushort c;
RTCTimer tt;


	// clear buffer?
	if(clearBufferFirst) TURxFlush(port);
	
	// start timer for timeout
	RTCElapsedTimerSetup(&tt);
	
	if((n = strlen(expect)) >= sizeof(response_buf)) return 0;
	if(strlen(send) > 0L) TUTxPutBlock(port,send,strlen(send),1000);

	 if(n == 0) return 1; // no expected message
	 i=0;
	 while(1) // read the first n bytes
 	 {
		if(RTCElapsedTime(&tt)/1000L > timeout) return -1;
		// get the next byte
		if(TURxQueuedCount(port) > 0) c=TURxGetByteWithTimeout(port,1);
		else continue;
		//printf("0x%x",c );  fflush(stdout);
	 	 response_buf[i] = c;
	 	 i++;
	 	 if(i == n) break;
	 }

	 response_buf[i] = '\0';
	 //printf("%s",response_buf); fflush(stdout);
     if(len > 0) strcpy(response,response_buf);

	// now read the buffer until the 'expect' is found and returns right away.
	 if(strncmp(response_buf, expect, (size_t)n))
	 {
		  /*  save n-1 characters, read a new one in */
		  n--;
		  do
		  {
		  //printf("%s\n",response);

				if(RTCElapsedTime(&tt)/1000L > timeout) return -1;
				// get the next byte
				if(TURxQueuedCount(port) > 0) c=TURxGetByteWithTimeout(port,1);
				else continue;
				
				// Shift the buffer to the left
				p = response_buf;
				q = response_buf + 1;
				for(i = 0;i < n;i++) *p++ = *q++;

				//printf("%c",c );  fflush(stdout);
		 //DBGMMP(if(c != 0x0a && c != 0x0d) printf("%c",c );
		 //else printf(",%02x",c ); fflush(stdout););
				*p++ = c;
				if(len>0 && strlen(response) < len-1) strcat(response,p-1); // keep only the first N bytes
				*p = 0;
		  } while(strncmp(response_buf, expect, (size_t)n+1L));
	 }

	 return 1;
}




# if 0
/*-------------------------------------------------------------------------
A slightly different verison of serDeviceChat() - returns a termChar-terminated 
line.

returns 1, if successful,
		-1, exceed timeout, no expected string found in response...
---------------------------------------------------------------------------*/
short serDeviceChatGetLine(
TUPort *port,
bool clearBufferFirst,
char *send,     // string to write to the modem.
char *expect,   // response string to wait for, must be 0 or >= 2 and < 31 chs.
char *response,     // line to be returned,   
short termChar,    // line-terminator
long timeout)  // maximum number of ms to wait for each ch.

{
register char	*p, *q;
register int	i, n;
char response_buf[128];
ushort c;

	// clear buffer?
	if(clearBufferFirst) TURxFlush(port);

	if((n = strlen(expect)) >= sizeof(response_buf)) return 0;
	if(strlen(send) > 0L) TUTxPutBlock(port,send,strlen(send),1000); 

	
	 if(n == 0) return 1; // no expected message
	 for(i = 0;i < n;i++) // read the first n bytes
	 {
	 	 if( (c=TURxGetByteWithTimeout(port,timeout)) < 0) return -1;
	 	 response_buf[i] = c;
	 }

	 response_buf[i] = '\0';
     strcpy(response,response_buf);

	//printf("serDeviceChatGetLine(): %s\n",response);


	// now read the buffer until the 'response' with LF found.
	 if(strncmp(response_buf, expect, (size_t)n))
	 {
		  /*  save n-1 characters, read a new one in */
		  n--;
		  do
		  {
		//printf("%s\n",response);
				/*
				** Shift the buffer to the left
				*/
				p = response_buf;
				q = response_buf + 1;
				for(i = 0;i < n;i++) *p++ = *q++;

				// get the next byte
				if( (c=TURxGetByteWithTimeout(port,timeout)) < 0) return -1;
		// if(c != 0x0a && c != 0x0d) printf("%c",c );
		// else printf(",%02x",c ); fflush(stdout);
				*p++ = c;
				strcat(response,p-1); 
				*p = 0;
		  } while(strncmp(response_buf, expect, (size_t)n+1L));
		  
	 }
	 // get the rest of the line
	 //if(serGetLineTimed(port,&response[strlen(response)],termChar,timeout) < 0) return -1;

	 return 1;
}



short serDeviceChat(
short port,
char *send,     // string to write to the serial device.
char *expect,   // partial string in the response to wait for.
char *response,     // total response string to be returned in timeout ms 
short timeout)  // maximum number of ms to wait for each ch.

{
	//printf("serDeviceChat(): sending %s\n",send);

	if(strlen(send) > 0L) U4SPuts(port,send);

	if(strlen(expect) == 0L) return 1; // no expected message
	
	serBufReadTimed(port,response,0,timeout);
	if(strstr(response,expect)) return 1;
	
	return -1;
}

#endif




int serPortGetLine(TUPort *,uchar *,int);
// check the buffer for terminator and read only if it exists.
// leave the rest in the buffer
// string will be null-terminated.  if not timed-out, returns # of bytes, else -1.
int serPortGetLine(
TUPort *tuPort,
uchar *datStr,
int TimeoutMilliSec)
{
int bytesAtSerialPort=0,oldCount=0,i;
RTCTimer rt1;

	RTCElapsedTimerSetup(&rt1);
	while(1)
	{
		if((bytesAtSerialPort=TURxQueuedCount(tuPort)) > 0)
		{	
			//printf("bytesAtSerialPort = %d\n",bytesAtSerialPort);
			// do we have a complete line?
			for(i=oldCount; i< bytesAtSerialPort; i++) 
				if(TURxPeekByte(tuPort,i) == '\n') 
				{
					TURxGetBlock(tuPort,datStr,(long)(i+1),0);  // only read to LF
					datStr[i+1]=0; // need to null-terminate !!!!
					//printf("\nbytes read: %s\n",datStr);
					return i+1;
				}
			oldCount=bytesAtSerialPort;
		}
		if(RTCElapsedTime(&rt1)/1000L > TimeoutMilliSec) return -1;
		RTCDelayMicroSeconds(10000L); // wait 10 ms
	}

}


int serPortInit(TUChParams *tp);
int serPortInit(
TUChParams *tp)
{
	tp=TUGetDefaultParams();
	//printf("rcv buffer size: %d\n",tp->rxqsz);
	tp->rxqsz=9600;
	TUSetDefaultParams(tp);

	tuport_imm = TUOpen(TPU_fromIMM, TPU_toIMM, BAUD_IMM, 0);
	if (tuport_imm == 0)
	{
		cprintf("\n!!! Error opening TU IMM channel, aborting\n");
		return 0;
	}
 
	tuport_modem = TUOpen(TPU_fromModem, TPU_toModem, BAUD_MODEM, 0);
	if (tuport_modem == 0)
	{
		cprintf("\n!!! Error opening TU modem channel, aborting\n");
		return 0;
	}
 
	tuport_ADCP = TUOpen(TPU_fromADCP, TPU_toADCP, BAUD_ADCP, 0);
	if (tuport_ADCP == 0)
	{
		cprintf("\n!!! Error opening TU ADCP channel, aborting\n");
		return 0;
	}
 
	tuport_SBE37 = TUOpen(TPU_fromSBE37, TPU_toSBE37, BAUD_SBE37, 0);
	if (tuport_SBE37 == 0)
	{
		cprintf("\n!!! Error opening TU SBE37 channel, aborting\n");
		return 0;
	}
 
#if 0
	// GPS
	tuport_gps = TUOpen(TPU_fromGPS, TPU_toGPS, BAUD_GPS, 0);
	if (tuport_gps == 0)
	{
		cprintf("\n!!! Error opening TU GPS channel, aborting\n");
		return 0;
	}
#endif 
 
	SCIRxSetBuffered(true);	// switch SCI to buffered receive mode
	SCITxSetBuffered(true);	// switch SCI to buffered transmit mode

	return 1;
}
