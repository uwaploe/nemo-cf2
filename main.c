/* -------------------------------------------------------------------------------
05-29-19:
	- MODEM_PWRUP_WAIT changed to 105000000L
02-01-13;
	- cell modem upload intervals at user-input.  If dial-up blocked by MMP or others, 
	  will dial-up at the next hour.
	- limit # of ifles uploaded to 30
	- if a file transfer fails, remove it from the queu, end session.
09-27-12:
	- Freewave replaced by cell modem.  For some reason kermitTxfer() would upload a 
	  file OK, but returns with a failure code.  After much playing around, we turned 
	  the DEBUG in Kermit on and it started working fine miraculously!
01-24-12:
	- ADCP: # of bytes to download increased from 1500 to 2500
	- MMP: timeout increased from 6 to 10 secs when waiting for REQNEW response
04-11-11: - upload data at X min past the hour
		  - removed the 2nd queue, zip MMP files as we get them and then queue them.
02-24-11: added another queue for zipped files.  So we zip all files first, then upload.
02-22-11: kermit txfer failed for larger files. P_PKTLEN in 'kermit.h' changed to 
		  512 bytes and working OK again.

Kermit library: 'kermit.lib', see C:\PROJECTS\NEMO mooring.10\src\kermit.lib
gzip library: 'gzip.lib', see C:\PROJECTS\NEMO mooring.10\src\gzip.lib:
	   
CF2:
	- to auto-exec at picoDOS in case of crash:
		- boot pico.
		- have an 'autoexec.bat' file containing the name of the program (itop).
	- low-power sleep: PWRSuspendSecs()
		- to get out of low-power sleep, do one of the following:
			1. remove jumper 1
	  		2. ground pin 38 (/WAKE)
	  		3. disconnect battery (insert end of a thin tie-wrap under 
	  		   the battery to break the connection).
		- CF2 restores everything, except TPU UART stuff, after PWRSuspendSecs().
		- for unknown reason, the CF2 may wake up from low-power sleep randomly
	- generating a library:
		- set up a folder where the library is to be created
		- file:new project, CW9 palm stationary
		- enter name and folder path, click OK
		- expand 'variations', select CFxLibrary
		- if using more than 1 library, use a diff name for each 'void libfunct(void)' ..		
		
MMP:

- idiosyncracies:
	- MMP response time to '#01REQNEW' got longer after many profiles, so 
	  increased time-out in serBufReadTillExpectedStringTimed().
	  formula for time delay:
	  Seconds = 2 + (CurrentProfileNumber x .012 x (# of files to upload))	
	  
	  to avoid increasing long dely time, enable the file delete feature. 

- If the disk space or the directory-entry limit is exceeded, the new data will be 
  ignored and the old data will be retained. (The deployment will actually terminate.)

- launch sequence:

	Proceed with the deployment (Yes/No) [N] ? y


	 >>> Initializing autonomous operation <<<

	   Do NOT remove the communication cable
	     until initialization is complete.

	 Setting motor to Free Wheel during launch . . . done.

	 Powering on the UIM . . .done.  <<<=== sends out a wake-up tone !!!!

	 Initializing data pointers and status flags . . . done.

	 Initializing flash card . . .
	   Deleting all previous data files
	     (process may take several minutes) . . . .

	 Deleting all files                                      . . .
	   Initializing flash pointers . . .
	   Creating DEPLOY.DAT . . .
	   Creating PROFILES.DAT . . .
	   Creating IRQ_XCPT.LOG . . .
	   Creating LASTSENT.DAT . . .
	 Flash card initialization complete.

	 System is ready to deploy.

	 Remove communication cable, apply dummy
	 plug to communications port, and attach
	 faired bottom cap to vehicle.

	 Powering off the UIM before dive 0

	 01/11/2011 16:06:07

	 Initializing CTD logging pointers . . .
	 ...
	 ...
	

IMM setup:
	- command terminator:\r\n
	- Setconfigtype=1 twice to change configuration
	- getcd: get status of IMM, should be (automatically after setting configtype=1):
		<ConfigurationData DeviceType='SBE90554 IMM' SerialNumber='70000957'>
		<Settings ConfigType='2'
		DebugLevel='1'
		BaudRate='9600'
		HostID='Host ID not set'
		GdataStr='GDATA'
		HostPrompt='x'
		ModemPrompt='IMM>'
		DeviceID='0'
		EnableHostFlagWakeup='0'
		EnableHostFlagConfirm='0'
		EnableHostFlagTerm='0'
		EnableSerialIMMWakeup='1'
		EnableHostPromptConfirm='1'
		EnableHostServeOnPwrup='0'
		EnableAutoIMFlag='1'
		EnablePrompt='1'
		EnableHostWakeupCR='1'
		EnableHostWakeupBreak='0'
		EnableEcho='1'
		EnableSignalDetector='1'
		EnableToneDetect='1'
		EnableFullPwrTX='0'
		EnableBackSpace='1'
		EnableGDataToSample='0'
		EnableStripHostEcho='0'
		EnableBinaryData='1'
		SerialType='1'      <<-- 1=RS232, 0=level logic
		TermToHost='254'
		TermFromHost='254'
		SerialBreakLen='5'
		MaxNumSamples='40'
		GroupNumber='0'
		THOST0='0'
		THOST1='5'
		THOST2='1000'
		THOST3='12000'
		THOST4='500'
		THOST5='5'
		TMODEM2='500'
		TMODEM3='18000'
		TMODEM4='100'	
	- global command MMDDYY=xxxxxx & HHMMSS=xxxxxx will set all sensors to the same time

SBE37SM (9600 baud):
	- SBE37 C,T,P: #iiformat=2 (p.37) : (  0.0086,  23.2794,   -0.069, 15:52:49, 10-07-2009\r\n)
	- #iids:
		SBE37SM:
		SBE37-IM V 2.3b  SERIAL NO. 6622    11-20-2009  16:59:40
		not logging: received stop command
		sample interval = 60 seconds
		samplenumber = 110513, free = 80137
		store time with each sample
		do not transmit sample number
		A/D cycles to average = 4
		internal pump is installed
		temperature = 20.32 deg C	
			
Power: (@ 12V)
	CF2 sleep: 1mA
	CF2 awake @ menu prompt: 67mA

--------------------------------------------------------------------------------- */

//#define NO_SUSPEND	// use RTCDelayMicroSeconds() to simulate low-power sleep


#include	<cfxbios.h>		// Persistor BIOS and I/O Definitions
#include	<cfxpico.h>		// Persistor PicoDOS Definitions

#include	<ctype.h>
#include	<errno.h>
#include	<stdarg.h>
#include	<stddef.h>
#include	<math.h>
#include	<string.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>

#include	"constant.h"
#include	"menu.h"
#include	"prototype.h"


TUPort *tuport_imm, *tuport_modem,*tuport_ADCP,*tuport_SBE37;
short IDmooring;
// modemUploadTime is on the minutes past the hour
long queryIntervalSBE3xIM=15,modemUploadTime=5; // minutes, converted to seconds later
char startDateTimeStr[25];
long modemUploadInterval=1;  // interval in hours



short SBEsamplesToUpload;
TUChParams *tp;



IEV_C_PROTO(spurious_handler);
IEV_C_FUNCT(spurious_handler)
{
	# ifndef LINUX_JHD
	# pragma unused(ievstack)
	# endif
}



int main(void)
{
long baud = 9600L;
int i,j,id;
char cmdStr[80],fname[16],fnameGz[16],key;
time_t nowSecs,secsToSleep,fileSecs,diffSecs,secsToSleep1,secsToSleep2;
uchar *dataBuf;
long bufSize,loops;
FILE *fp,*fpUp;
RTCTimer tt;
bool timeToUploadModem=false;
char fpUpMode[4],*p;
short upldRet,rebootCount,result;
short IOpin_pwr_IMM,IOpin_pwr_ADCP,IOpin_pwr_modem,IOpin_pwr_SBE37;
int wokeFromSuspend=1;  // set to 1 so IMM will be set properly the first time
int filesInQueue=0,numFilesUploaded=0;  
time_t secsAtLastUpload=0;

 	
	IEVInsertCFunct((IEVCWrapper *)&spurious_handler, spuriousInterrupt);

	TUInit(calloc, free);		// give TU manager access to our heap
	
	// find out default settings & modify
	tp=calloc(1, sizeof(TUChParams));

	serPortInit(tp);  // all serial ports
	IDmooring=1;
	
 	
	IOpin_pwr_IMM=TPUPinFromChan(TPU_PWR_IMM); // IMM hard-wired ON
	// IOpin_pwr_ADCP=TPUPinFromChan(TPU_PWR_ADCP); // external power
	IOpin_pwr_modem=TPUPinFromChan(TPU_PWR_MODEM);
	// IOpin_pwr_SBE37=TPUPinFromChan(TPU_PWR_SBE37); // external power
			
#ifdef NO_SUSPEND
	printf("\n !!! This is a test version using RTCElapsedTime() to simulate power suspend.!!!\n\n");
#endif

	autoExec(1); // create autoexec.bat for auto-start if crash into picoDOS
	rebootCount=rebootCountRW(FN_REBOOTCOUNT,false); // read the # of reboots
	printf("Program rebooted %d times\n",rebootCount);
	
	strcpy(startDateTimeStr,timeStamp());  //use current time in case fopen error
	printf("current time= %s\n",startDateTimeStr);
	configFileRead(FN_CFG);
	
	freemem();
	initQueue();
	
	printf("\n\nReal-time mode will start in 10 sec, hit any key to get into menu...\n");
	for(i=10; i>0; i--) 
	{
		printf("%d\n",i);
		RTCDelayMicroSeconds(1000000L);
		if(kbhit()) 
		{
			kbflush();
			menu_main(" " CODENAME ", Ver. " VERSION ", Main Menu, Build: " __DATE__ ", " __TIME__,
				"Start deployment");
			configFileWrite(FN_CFG);
			goto START;
		}
	}
	



START:
	
	printf("\nInterval to query SBE37 & ADCP=%ld min\n"
		"Time to upload via modem: %ld min past hour\n\n",
		queryIntervalSBE3xIM,modemUploadTime);
	queryIntervalSBE3xIM*=60; // cvt to seconds
	modemUploadTime*=60;
	printf("Current time: %s \n\n",timeStamp());
	
	printf("Hit CTL-Q while awake to exit to picoDOS...\n\n");
	
		

	// IMM must be in ConfigType=2 to detect wakeup tone
	// try IMMsetConfigType2() 2 times. Continue even if not successful.
	for(i=0; i<2; i++)
	{
		result=IMMsetConfigType2(tuport_imm);
		if (result > 0)
		{
			printf("%s - IMM in sleep..\n",timeStamp());
			break;
		}
	}
	SCITxWaitCompletion();
	
	
	secsAtLastUpload=0;  // always call within the 1st hour after deployment
	// now into mission loop
	for(loops=0; ; loops++)
	{
		RTCDelayMicroSeconds(1000000L);		
		// way to exit & get out of sleep; for use during development
		if(kbhit()) 
		{
			key=getch();
			if(key== 0x11) quit();  // ctl-q
		}
		
		nowSecs=time(NULL);
		secsToSleep1=queryIntervalSBE3xIM-(int)fmod((double)nowSecs,(double)queryIntervalSBE3xIM);
		secsToSleep2=modemUploadTime-(int)fmod((double)nowSecs,3600.);
		//printf("secsToSleep: %ld, %ld\n",secsToSleep1,secsToSleep2);
		timeToUploadModem=false;
		if( secsToSleep2<= 0) secsToSleep=secsToSleep1;
		else
		{
			if(secsToSleep1 < secsToSleep2) secsToSleep=secsToSleep1;
			else 
			{
				if((time(NULL)-secsAtLastUpload) > (modemUploadInterval*3600L-600L))
				{
					timeToUploadModem=true;
					secsToSleep=secsToSleep2;
				}
				else
				{
					secsToSleep=secsToSleep1;
					timeToUploadModem=false;
				}

			}
		}
		printf("%s - CF2 sleeping for %ld secs ..\n----------------------------\n",
			timeStamp(),secsToSleep);

		SCITxWaitCompletion();
		/* typedef enum	{
				WakeOnTimeout,  	WakeTmtOrWAKEFall,
				WakeTmtOrCFChg,		WakeTmtWAKECFChg	}	WhatWakesSuspend;
		   typedef enum	{
				WokeFromTimeout,  	WokeTmtOrWAKEFall,		WokeTmtOrCFChg,
				WokeTmtWAKECFChg,	WokeNeverSuspending }	WhatWokeSuspend;
		*/
		wokeFromSuspend=PWRSuspendSecs(secsToSleep,true,WakeTmtOrWAKEFall);
		serPortInit(tp);
		

		// woke up
		if(timeToUploadModem==false)
		{
			if(wokeFromSuspend==WokeFromTimeout) 
			{
				printf("%s - main(): woke up on time-out\n",timeStamp());
				
				// grab ADCP & SBE37 data & queue the files.
				ADCPtxfer(tuport_ADCP,&filesInQueue);
				SBE37txfer(tuport_SBE37,&filesInQueue);						
			}
			else if(wokeFromSuspend==WokeTmtOrWAKEFall) 
			{
				printf("%s - main(): woke up on tone:\n",timeStamp());
						
				// wait for all the "WAKE-UP TONE DETECTED" messages from IMM to finish
				// tone lasts ~2.5 secs
				RTCDelayMicroSeconds(4000000L);		
			
				TURxFlush(tuport_imm);

				// IMM must be in configtype=1 (SIM simulation mode) to talk to UIM!!!
				// try IMMsetConfigType1() 2 times.  Continue even if not successful.
				for(i=0; i<2; i++)
				{
					result=IMMsetConfigType1(tuport_imm);
					if (result > 0)
					{
						printf("IMM powered up..\n");
						break;
					}
				}
				
				MMPtxfer(tuport_imm,&filesInQueue); // txfer & store files & put them in queue for upload
				freemem();

				// IMM must be in ConfigType=2 to detect wakeup tone
				// try IMMsetConfigType2() 2 times. 
				for(i=0; i<2; i++)
				{
					result=IMMsetConfigType2(tuport_imm);
					if (result > 0)
					{
						printf("%s - IMM in sleep..\n",timeStamp());
						break;
					}
				}
				SCITxWaitCompletion();
			} //if(i==WokeFromTimeout ...

		}
		else
		{ // time to upload...
			
			printf("# of files in queue to upload: %d\n",filesInQueue);
			if( filesInQueue > 0)
			{
				secsAtLastUpload=time(NULL);
				numFilesUploaded=0;
				// now upload files in queue
				PIOSet(IOpin_pwr_modem);
				RTCDelayMicroSeconds(MODEM_PWRUP_WAIT);		
				upldRet=login(tuport_modem);
				
				if(upldRet == 0) 
				{
					sprintf(cmdStr,"main(): Can't login to server...\n");
					logMsg(cmdStr);
				}
				else 
				{
					while(filesInQueue>0 && numFilesUploaded < MAX_FILES_TO_UPLOAD)
					{
						deQueue(&i,fname); // i should be the same as filesInQueue
						filesInQueue--; // next file, even if txfer fails to avoid corrupt file problem

						// check to see if we can open the file first.  if not, skip						
						if((fp=fopen(fname,"r")) == NULL) 
						{
							printf("main(): queued file '%s' open error!\n",fname);
							goto NEXT_FILE;
						}
						fclose(fp);
						
						printf("Uploading '%s'.., ",fname);  fflush(stdout);
						nowSecs=time(NULL);
						upldRet=fileTxfer(tuport_modem,fname);
						
						// delete zipped file from flash, irregardless of success or failure
						if(strstr(fname,".gz"))
						{
							sprintf(cmdStr,"del %s",fname);
							nowSecs=time(NULL);
							execstr(cmdStr);
							printf("file deleted in %ld secs\n",time(NULL)-nowSecs);
						}
						
						if(upldRet == 0)
						{
							sprintf(cmdStr,"main(): File txfer failed...\n");
							logMsg(cmdStr);
							break;
						}
						printf("uploaded in %ld secs\n",time(NULL)-nowSecs);
						
NEXT_FILE:;
						RTCDelayMicroSeconds(2000000L);	// wait for 2 sec fro >IKSD prompt
						numFilesUploaded++;
					} // while(...
					
					printf("logging out..\n");
					upldRet=logout(tuport_modem);
				}
				PIOClear(IOpin_pwr_modem);
				// done uploading
				
			} // if( filesInQueue ...
		} // if(timeToUploadmODEM==false).. / else
		
			
	} //for(loops=0, ...

	
EXIT: // never exits...
	if (tuport_imm)	TUClose(tuport_imm);
	BIOSResetToPicoDOS();	// full reset, but jump to 0xE10000 (PicoDOS)
	return 0;

}	
