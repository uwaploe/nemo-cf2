#include	<cfxbios.h>		// Persistor CF1 System and I/O Definitions
#include	<cfxpico.h>		// Persistor CF1 PicoDOS Definitions  

#include	<assert.h>
#include	<ctype.h>
#include	<errno.h>
#include	<float.h>
#include	<limits.h>
#include	<locale.h>
#include	<math.h>
#include	<signal.h>
#include	<stdarg.h>
#include	<stddef.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>

#include	"prototype.h"
#include	"constant.h"



static char tmpChar[80];
static char *subStr[7];


extern short IDmooring;
extern long queryIntervalSBE3xIM,modemUploadTime; // minutes, converted to seconds later
extern long modemUploadInterval; // hrs
extern char startDateTimeStr[30];  // do not use *startDateTimeStr !


#if 0

/*---------------------------------------------------------------------
compute the checksum of a null-terminated NMEA message $ACCLK,xxxx,yyyy.
checksum is the XOR of the bytes after $ and before *.  
If the input NMEA string has checksum, this function will compare the computed
and the received checksums.
If the input NMEA string doesn't have a checksum (no *HH), it will be appended
to the end of the string as *HH.
returns:0 - OK
		-1: checksums do not jive
---------------------------------------------------------------------*/
short NMEAchecksumCmpOrAppend(
char *msg) // input, output
{
short i,n,n0;
ushort chksumCmp,chksumRcv;
char *p1, *p2;

	
	p1=strchr(msg,'$');
	// find the pos of the last byte to compute
	if((p2=strchr(msg,'*'))) {
		n=p2-msg;  
		sscanf(p2+1,"%2x",&chksumRcv);  
	}
	else 
		n=strlen(msg);
	
	n0=p1-msg+1;
	chksumCmp = msg[n0];
	for(i=n0+1; i<n; i++) chksumCmp = chksumCmp ^ msg[i] ;
	
	if(!p2) {
		sprintf(&msg[n],"*%02x",(chksumCmp&0xff));
		return 0;
	}
	else {
		if(chksumCmp == chksumRcv) return 0;
		else return -1;
	}
}
#endif

void logMsg(
char *msg)
{
FILE *fp;
long t0;
	
	printf("%s",msg);
	if((fp=fopen(FN_LOG,"at")) == NULL) {
		printf("Unable to open logfile!\n");
		return;
	}
	
	t0=time(NULL);
	strftime(tmpChar, sizeof(tmpChar)-1, "%m/%d %H:%M:%S-", localtime(&t0));
	fwrite(tmpChar,1L,strlen(tmpChar),fp);
	fwrite(msg,1L,strlen(msg),fp);
	fclose(fp);
}

	

void powerOff(
short pin)
{
	PIOSet(pin); // high to turn off becasue of an inverter in the circuit
}


void powerOn(
short pin)
{
	PIOClear(pin); // low to turn on 
}


char *timeStamp(void)
{
time_t t0;
static char tmpChar[20];

	t0=time(NULL);
	strftime(tmpChar, sizeof(tmpChar)-1, "%m/%d/%y %H:%M:%S", localtime(&t0));
	return tmpChar;
}


	
/* freemem(): find out how much memory is available, returns # of bytes 
seems to return multiples of 8 bytes??? */
long freemem(void)
{
long i,memsize,incr;
int *datastart;

	/* start at a meg and work down, each time by a factor of 2 */
	for (i = 0x100000; i != 0; i >>= 1)		
		if (datastart = malloc(i)) break;
	free(datastart);
	
	/* must be between i*2 and i.
	try nudging dn in smaller steps  */
	for (memsize = i * 2; memsize > 0; memsize -= 1024)	
		if (datastart = malloc(memsize)) break;
	free(datastart); 
	//printf("memsize= %ld\n",memsize);

	i=memsize;
	incr=1024;
	while(incr>= 1)
	{
		for (memsize = i + incr; memsize > 0; memsize -= incr)	
			if (datastart = malloc(memsize)) break;
		free(datastart); 
		//printf("memsize= %ld\n",memsize);
		incr /=2;
		i=memsize;
	}
	
	printf("\navailable memory= %ld\n",memsize);
	return(memsize);
}



// convert date time string to seconds since 1/1/1970
time_t dateTimeStrToSeconds(
char *dateStr,		// mm-dd-yyyy
char *dateFormat, 	// "%d-%d-%d", or match dateStr
char *timeStr,		// hh:mm:ss
char *timeFormat)  	// "%d:%d:%d", or match timeStr
{
short mm,dd,yy,hh,min,ss;
struct tm dt;

	sscanf(dateStr,dateFormat,&mm,&dd,&yy);	
	sscanf(timeStr,timeFormat,&hh,&min,&ss);	
	
	// now convert date-time to seconds since 1/1/1970
	dt.tm_year = yy-1900; // yes, 1900!
	dt.tm_mon = mm-1;
	dt.tm_mday = dd;
	dt.tm_hour= hh;
	dt.tm_min = min;
	dt.tm_sec = ss;
	return(mktime(&dt));  // mktime() returns -1 if error
}


		
void configFileRead(
char *fname)
{
FILE *fp;
char tmp[30];
	
	if((fp=fopen(fname,"rt")) == NULL) printf("Config file read open error!\n");
	else 
	{

		fscanf(fp,"%ld\n",&queryIntervalSBE3xIM);
		fscanf(fp,"%ld\n",&modemUploadInterval);
		fscanf(fp,"%ld\n",&modemUploadTime);
		fclose(fp);
	}
}


		
void configFileWrite(
char *fname)
{
FILE *fp;

	if((fp=fopen(fname,"wt")) == NULL) printf("Config file write open error!\n");
	else 
	{
		fprintf(fp,"%ld\n",queryIntervalSBE3xIM);
		fprintf(fp,"%ld\n",modemUploadInterval);
		fprintf(fp,"%ld\n",modemUploadTime);
		fclose(fp);
	}
}



short rebootCountRW(
char *fname,
bool reset) // true=reset count to 0
{
FILE *fp;
short n=0;

	
	if((fp=fopen(fname,"r+")) == NULL  // has to be r+ for rewind() to work!!
		&& (fp=fopen(fname,"w")) == NULL) 
	{
		printf("Reboot count file open/create error!\n");
		return -1;
	}

	if(!reset) fscanf(fp,"%d",&n);
	else n=0;	
	n++;
	rewind(fp);
	fprintf(fp,"%d\n",n);
	fclose(fp);
	return n;
}

