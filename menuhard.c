#include	<cfxbios.h>		// Persistor CF1 System and I/O Definitions
#include	<cfxpico.h>		// Persistor CF1 PicoDOS Definitions  

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
//#include <conio.h>

#include "prototype.h"
#include "constant.h"
#include "menu.h"


extern short IDmooring;
extern TUPort *tuport_imm, *tuport_modem,*tuport_ADCP,*tuport_SBE37;
extern long queryIntervalSBE3xIM,modemUploadTime; // minutes, converted to seconds later
extern char startDateTimeStr[25];
extern TUChParams *tp;
extern long modemUploadInterval;  // interval in hours


static char *backToPrevMenu="Return to previous menu";

static char SetSBESampleIntervalStr[80],SetQueryIntervalStr[80],SetModemUploadIntervalStr[80],
	SetModemUploadTimeStr[80],toggleModemPowerStr[80],toggleIMMPowerStr[80],
	toggleSBE37PowerStr[80],toggleADCPPowerStr[80];
int pinState_IMM_PWR= -1,pinState_MODEM_PWR= -1,pinState_GPS_PWR= -1,pinState_SBE37_PWR=-1,
	pinState_ADCP_PWR=-1;


static char *IMMcmds=
	"\n\nIMM commands:\n"
	"  getcd\n"
	"  gethd\n"
	"  getsd\n"
	"  pwron: talk to MMP when it's in bench test mode\n"
	"\nMMP/UIM commands:\n"
	"  !01ds\n"
	"\nMMP/UIM commands:\n"
	"  #01reqdir\n"
	"  #01reqnew\n"
	"  #01reqeod: MMP powers off UIM\n"
	"  #01reqack\n";

static char *MODEMcmds=
	"\n\nlogging into server:\n"
	"   user: nemo\n"
	"   passwd: chabba\n\n";
	
static char *ADCPcmds=
	"\n\nADCP commands:\n"
	"ce: download the last ensemble.\n"
	"cs: start pinging.\n"
	"ctl-q to exit\n";
	
static char *SBE37cmds=
	"\n\nSBE37 commands:\n"
	"  ds/getcd/getsd: get configuration/status\n"
	"  Interval=xx: set sample interval (secs)\n"
	"  BaudRate=xxxx: set baud rate\n"
	"  TxSampleNum=Y/N: txmit sample number?\n"
	"  OutputFormat=0/1/2: set output data format\n"
	"     0: 524276,  2886.656, 785053, 2706, 20 Aug 2008, 09:01:34\n"
	"     1: 8.5796,  0.15269,  531.316,   1.1348, 20 Aug 2008, 09:01:44\n"
	"     2: converted decimal data in XML \n"
	"  InitLogging: clear memory\n"
	"  MinCondFreq=xxx: min cnductivity to run pump\n"
	"  storetime=y/n\n"
	"  startnow\n"
	"  startDateTime=mmddyyyyhhmmss (must be followed by 'startlater')\n"
	"  startlater: start logging at programmed date-time\n"
	"  stop: stop logging\n"
	"  resumelogging\n"
	"  sl: send last sample\n\n";


static MenuEntry main_menu[] = {
{"Exit to picoDOS",  quit, 0},
{"Check available memory",  (MenuCallback) freemem, 0},
{"Test CF2 power suspend",  (MenuCallback) testPwrSuspend, 0},
//{"Reset reboot count",  (MenuCallback) resetReboot, 0},
{"Set CF2 clock", (MenuCallback) setClock, 0 },
{SetQueryIntervalStr, (MenuCallback) setSBEUploadInterval, 0 },
{SetModemUploadIntervalStr, (MenuCallback) setModemUploadInterval , 0 },
{SetModemUploadTimeStr, (MenuCallback) setModemUploadTime , 0 },
{"Enter IMM/MMP sub-menu", (MenuCallback)  menu_imm, 0  },
{"Enter cell modem sub-menu", (MenuCallback)  menu_modem, 0  },
{"Enter ADCP sub-menu", (MenuCallback)  menu_adcp, 0  },
{"Enter SBE37 sub-menu", (MenuCallback)  menu_SBE37, 0  },
//{"Set deployment start date/time",  (MenuCallback) setStartDateTime, 0},
{"Start deployment!",  (MenuCallback) exitMenu, 0},
};


static MenuEntry imm_menu[] = {
//{toggleIMMPowerStr, (MenuCallback) togglePower, TPU_PWR_IMM  },
{"Direct comm with IMM",  (MenuCallback) IMMcomm, 0 },
{"Return to main menu",  (MenuCallback) exitMenu, 0 },
};


static MenuEntry adcp_menu[] = {
//{toggleADCPPowerStr, (MenuCallback) togglePower, TPU_PWR_ADCP },
{"Initialize ADCP", (MenuCallback) ADCPinit, 0  },
{"Send BREAK signal to ADCP", (MenuCallback) ADCPbreakSignal, 0  },
{"Direct comm with ADCP",  (MenuCallback) ADCPcomm, 0 },
{"Return to main menu",  (MenuCallback) exitMenu, 0 },
};

static MenuEntry SBE37_menu[] = {
//{toggleSBE37PowerStr, (MenuCallback) togglePower, TPU_PWR_SBE37 },
{"Direct comm with SBE37",  (MenuCallback) SBE37comm, 0 },
{"Return to main menu",  (MenuCallback) exitMenu, 0 },
};


static MenuEntry modem_menu[] = {
{toggleModemPowerStr, (MenuCallback) togglePower, TPU_PWR_MODEM },
{"Direct comm with cell modem (wait ~80s after power-up)",  (MenuCallback) MODEMcomm, 0 },
{"Auto-login (wait ~80s after power-up)",  (MenuCallback) autoLogin, 0 },
{"Test kermit file txfer after login ",  (MenuCallback) testFileTxfer, 0 },
{"Return to main menu",  (MenuCallback) exitMenu, 0 },
};


void exitMenu(void)
{
// when user chooses this item, show_menu() will exit the menu....
}


void resetReboot(void)
{
	rebootCountRW(FN_REBOOTCOUNT,true);
}


void autoExec(
int mode)
{
FILE *fp;

	if(mode ==0) execstr("del autoexec.bat");
	else
	{
		if((fp=fopen("autoexec.bat","wt"))==NULL)
			printf("Unable to create 'autoexec.bat' file...\n");
		else
		{
			fprintf(fp,"%s\n",CODENAME);
			fclose(fp);
			printf("\n!!! 'Autoexec.bat' created to re-start '%s' if crash into picoDOS !!!\n\n",CODENAME);
		}
		
	}
}


int testFileTxfer(void)
{
FILE *fp;
char fname[16];
time_t nowSecs;
bool zipYN;
int upldRet,i;
long nn=1;

	QRstring("\nFile to upload? ","%s",false,fname,12);
	if( (fp=fopen(fname,"r")) ==NULL)
	{
		printf("File '%s' doesn't exit ...\n",fname);
		return 0;
	}

	QRlong("\nHow many times (<20)? ","%ld",true,&nn,1,20);

// zipping doesn't seem to save time overalll
#if 0
	zipYN=QRconfirm("Zip the file (Y/N)? ",true,true);
	
	if(zipYN)
	{
		strcpy(fnameGz,fname);
		p=strchr(fnameGz,'.'); 
		sprintf(p+3,"z");
		printf("file to upload: %s\n",fnameGz);
		// now zip the binary file 'upload.bin' to 'yymmddhh.gz' 
		// gzip() returns 0 if success, else -1
		nowSecs=time(NULL);
		upldRet=gzip(fname,"rb",fnameGz,"wb",true );
		printf("zipped in %ld secs\n",time(NULL)-nowSecs);
		if(upldRet < 0) printf("gzip error, file will be txferred unzipped !\n");	
		else strcpy(fname,fnameGz);

	}
#endif
	
	for(i=0; i<nn; i++)
	{
		nowSecs=time(NULL);
		if(fileTxfer(tuport_modem,fname) > 0) printf("\nfile '%s'txferred...\n",fname);
		else  printf("\nfile txfer failed...\n");
		printf("Elapsed time= %ld secs\n",time(NULL)-nowSecs);
		RTCDelayMicroSeconds(2000000L);	// wait for >IKSD prompt
	}
	return 1;

}


void ADCPbreakSignal(void)
{
	TUBreak(tuport_ADCP, 70);
}


#if 0
void autoInitModem(void)
{
	modem_init(tuport_modem);
}



void autoDialModem(
short which)
{
	printf("\n\nAfter dialing, do 'Direct comm with modem' to log in...\n\n");
	if(which) modem_dial(tuport_modem,telnum_RUDICS, true);  // is this a RUDICS dial-up (true) or regular modem (false)?
	else modem_dial(tuport_modem,telnum_LANDLINE, false);
}

#endif


void autoLogin(void)
{
	login(tuport_modem);
}


void menu_imm(void)
{
    show_menu("     IMM Menu", imm_menu, 
             sizeof(imm_menu)/sizeof(MenuEntry), backToPrevMenu);
}

void menu_adcp(void)
{
    show_menu("     ADCP Menu", adcp_menu, 
             sizeof(adcp_menu)/sizeof(MenuEntry), backToPrevMenu);
}


void menu_modem(void)
{
    show_menu("      Modem Menu", modem_menu, 
             sizeof(modem_menu)/sizeof(MenuEntry), backToPrevMenu);
}

void menu_SBE37(void)
{
    show_menu("      SBE37 Menu", SBE37_menu, 
             sizeof(SBE37_menu)/sizeof(MenuEntry), backToPrevMenu);
}


void testPwrSuspend(void)
{
static long secs=10L;
	QRlong("\nHow long to suspend the power (1-180 sec): ","%ld",
		true,&secs,0,181);
	printf("suspending power for %ld secs...\n",secs);
	RTCDelayMicroSeconds(500000L);
	PWRSuspendSecs(secs,true,0);
	serPortInit(tp);

}


void setStartDateTime(void)
{
	QRstring("\nEnter starting date & time (mm/dd/yyyy hh:mm:ss): ","%s",
		true,startDateTimeStr,30);
	//printf("Start time: %s\n",startDateTimeStr);
}



void setSBEUploadInterval(void)
{
	QRlong("\nInterval to query SBE3xIM data (1-180 integer min): ","%ld",
		true,&queryIntervalSBE3xIM,0,181);
	sprintf(SetQueryIntervalStr,"Enter interval to query SBE3xIM data [%ld min]",
		queryIntervalSBE3xIM);
	configFileWrite(FN_CFG);
}


void setModemUploadInterval(void)
{
	QRlong("\nInterval to dial-up to upload (1-24 hrs): ","%ld",
		true,&modemUploadInterval,0,25);
	sprintf(SetModemUploadIntervalStr,"Enter interval to dial-up via cell modem [%ld hrs]",
		modemUploadInterval);
	configFileWrite(FN_CFG);
}


void setModemUploadTime(void)
{
	QRlong("\nTime to upload data via cell modem (0-59 integer min,\n"
	"make sure no conflict with SBE/ADCP query time): ","%ld",
		true,&modemUploadTime,0,59);
	sprintf(SetModemUploadTimeStr,"Enter minutes past hour to upload data via cell modem[%ld min]",
		modemUploadTime);
	configFileWrite(FN_CFG);
}




void readIOpin(short pin)
{
short state;
	
	printf("\nHit any key to quit...\n");
	while(1) 
	{
		state=PIORead(pin);
		printf("Pin %d: %d\n",pin,state);
		
		if(kbhit()) break;
		RTCDelayMicroSeconds(250000L);
	}	
	
	kbflush();
	
}
	
	
// ---item under main menu------------------
void menu_main(char *title,char *from)
{
	//sprintf(SetMooringIDStr,"Enter mooring ID # [%d]",IDmooring);
	sprintf(SetQueryIntervalStr,"Enter interval to query SBE37/ADCP data [%ld min]",
		queryIntervalSBE3xIM);
	sprintf(toggleModemPowerStr,"Toggle modem power [%s]",pinState_MODEM_PWR>0? "on":"off");
	sprintf(SetModemUploadIntervalStr,"Enter interval to dial-up via cell modem [%ld hrs]",
		modemUploadInterval);
	sprintf(SetModemUploadTimeStr,"Enter minutes past hour to upload data via cell modem [%ld min]",
		modemUploadTime);
	//sprintf(toggleIMMPowerStr,"Toggle IMM power [%s]",pinState_IMM_PWR>0? "on":"off");
	//sprintf(toggleSBE37PowerStr,"Toggle SBE37 power [%s]",pinState_SBE37_PWR>0? "on":"off");
	//sprintf(toggleADCPPowerStr,"Toggle ADCP power [%s]",pinState_ADCP_PWR>0? "on":"off");
	
	show_menu(title, main_menu, sizeof(main_menu)/sizeof(MenuEntry), from);
}



void quit(void)
{
	autoExec(0); // remove autoexec.bat
	//rebootCountRW(FN_REBOOTCOUNT,true);
	BIOSResetToPicoDOS();
}



void togglePower(
short pin)
{
short IOpin;

	IOpin=TPUPinFromChan(pin);
	if(pin == TPU_PWR_MODEM) 
	{
		pinState_MODEM_PWR = -pinState_MODEM_PWR;
		sprintf(toggleModemPowerStr,"Toggle modem power [%s]",pinState_MODEM_PWR>0? "on":"off");
	}
	else if(pin == TPU_PWR_IMM)
	{
		pinState_IMM_PWR = -pinState_IMM_PWR;
		sprintf(toggleIMMPowerStr,"Toggle IMM power [%s]",pinState_IMM_PWR>0? "on":"off");
	}
	else if(pin == TPU_PWR_SBE37)
	{
		pinState_SBE37_PWR = -pinState_SBE37_PWR;
		sprintf(toggleSBE37PowerStr,"Toggle SBE37 power [%s]",pinState_SBE37_PWR>0? "on":"off");
	}
	else if(pin == TPU_PWR_ADCP)
	{
		pinState_ADCP_PWR = -pinState_ADCP_PWR;
		sprintf(toggleADCPPowerStr,"Toggle ADCP power [%s]",pinState_ADCP_PWR>0? "on":"off");
	}


	PIOToggle(IOpin);
	// pin state must be saved in memory! 
	// do not read pin!  it will set pin to input.
}



void setTPUPin(
short pin)
{
char tmp[10],*subStr[3];
short ch,state;

	while(1) 
	{
		printf("\nEnter TPU #, 0/1 (CR to exit):"); fflush(stdout);
		consoleInput(tmp); 
		if(strlen(tmp) < 2) break;
		n_substr(",",2,tmp,subStr);
		ch=atoi(subStr[0])+21;  // convert to I/O pin #
		state=atoi(subStr[1]);
		if(state) PIOSet(ch);
		else PIOClear(ch);
	}	
}



void IMMcomm(void)
{
	printf("%s",IMMcmds);			  
	serDevIOCF2(tuport_imm,true); //IMM commands must be terminated with \r\n
}



void MODEMcomm(void)
{
	printf("%s",MODEMcmds);			  
	serDevIOCF2(tuport_modem,false);
}


void ADCPcomm(void)
{
	printf("%s",ADCPcmds);			  
	serDevIOCF2(tuport_ADCP,false);
}


void SBE37comm(void)
{
	printf("%s",SBE37cmds);			  
	serDevIOCF2(tuport_SBE37,false);
}


void ADCPinit(void)
{
short i;
char buf[30];
char *cmd[]={"CR1\r","CF11001\r","CH1\r",
	"EA0\r","EB0\r","ED150\r","ES35\r","EX11111\r","EZ1111101\r",
	"RI0\r",
	"WA50\r","WB0\r","WD111100000\r","WF88\r","WN45\r","WP13\r","WS50\r","WV175\r",
	"TE00:02:00.00\r","TP00:01.00\r",	"AZ\r","CK\r",
	""}; // the last string must be a null, used as a flag !!!
	
	TURxFlush(tuport_ADCP);
	
	for(i=0;;i++)
	{
		//printf("cmd #%d: %c\n",i,cmd[i][0]);
		if(cmd[i][0] == 0) break;
		serDeviceChatCF2(tuport_ADCP,true,cmd[i],">",1000,buf,25);
		if( strstr(buf,"ERR")) printf("%s\n",buf);
		else printf("parameter set: %s\n",cmd[i]);
	}
}



#if 0
void GPScomm(void)
{
	printf("%s",GPScmds);			  
	TURxFlush(tuport_gps);
	serDevIOCF2(tuport_gps,true); // GPS-15H commands must be terminated with \r\n
}


void SBE3xIMcomm(void)
{
char str[51],str2[50];
int len;

	printf("%s",IMMcmds);			  
	printf("Hit CR to exit...\n");
	while(1)
	{
		str[0]=0;
		QRstring("\n\nCommand to send: ","%s",true,str,50);
		if(str[0] == 0) break;
		len=strlen(str);
		str[len++]= '\r';  //QRstring will not pass \r
		str[len++]= '\n';
		str[len+2]= 0;
		
		serDeviceChatCF2(tuport_imm,true,str,"S>",10000L,str2,0);
	}
}


void SBE3xIMsetupAll(void)
{
int i;
char cmdStr[50],str2[50],mmddyy[10],hhmmss[10];

	mmddyy[0]=hhmmss[0]=0;
	QRstring("\nEnter starting MMDDYY: ","%s",true,mmddyy,6);
	QRstring("\nEnter starting HHMMSS: ","%s",true,hhmmss,6);
	printf("\n\n");

	for(i=1; i<=numSBEIMsensors; i++)
	{
		sprintf(cmdStr,"#%02dFormat=2\r\n",i);
		serDeviceChatCF2(tuport_imm,true,cmdStr,"S>",5000L,str2,0);
		RTCDelayMicroSeconds(1000000L);
		sprintf(cmdStr,"#%02dStartMMDDYY=%s\r\n",i,mmddyy);
		serDeviceChatCF2(tuport_imm,true,cmdStr,"S>",5000L,str2,0);
		RTCDelayMicroSeconds(1000000L);
		sprintf(cmdStr,"#%02dStartHHMMSS=%s\r\n",i,hhmmss);
		serDeviceChatCF2(tuport_imm,true,cmdStr,"S>",5000L,str2,0);
		RTCDelayMicroSeconds(1000000L);		
		sprintf(cmdStr,"#%02dStartLater\r\n",i);
		serDeviceChatCF2(tuport_imm,true,cmdStr,"S>",5000L,str2,0);
		RTCDelayMicroSeconds(1000000L);		
	}
	
}


void SBE3xIMstopAll(void)
{
int i;
char cmdStr[50],str2[50];


	serDeviceChatCF2(tuport_imm,true,"pwron\r\n","S>",5000L,str2,0);
	for(i=1; i<=numSBEIMsensors; i++)
	{
		sprintf(cmdStr,"#%02dstop\r\n",i);
		serDeviceChatCF2(tuport_imm,true,cmdStr,"S>",5000L,str2,0);
		RTCDelayMicroSeconds(1000000L);
	}
}
#endif


void setClock(void)
{
DateFieldOrder dfo=1;
struct tm *tm1;
time_t t0;
bool ret;

	tm1=malloc(sizeof(struct tm));
	t0=time(NULL);
	tm1=localtime(&t0);
	ret=QRdatetime("Enter time & date (mm/dd/yyyy hh:mm:ss, CTL-C to cancel):\n",1,true,false,tm1); 
	if(ret)	
	{
		RTCSetTime(mktime(tm1),0);
		printf("\n\nDate & time set to %s\n\n",timeStamp());
	}
	//free(tm1);
}

