// modified for testing with RDI workhorse ADCP
/******************************************************************************\
**	TUSimpleTerm.c			Persistor CF2 TPU UART Simple Terminal example
**	
**	Release:		2010/10/04		Changes to show several TU techniques
									by implementing a break signal:
		1. How to change the baud on-the=fly.
		2. How to generate a better baud match at higher baud rates.
		3. How to properly implement a complete TUTxWaitCompletion.
		4. How to generate a break character.
*****************************************************************************
**	
**	COPYRIGHT (C) 1995-2002 PERSISTOR INSTRUMENTS INC., ALL RIGHTS RESERVED
**	
**	Developed by: John H. Godley for Persistor Instruments Inc.
**	254-J Shore Road, Bourne, MA 02532  USA
**	jhgodley@persistor.com - http://www.persistor.com
**	
**	Copyright (C) 1995-2001 Persistor Instruments Inc.
**	All rights reserved.
**	
*****************************************************************************
**	
**	Copyright and License Information
**	
**	Persistor Instruments Inc. (hereafter, PII) grants you (hereafter,
**	Licensee) a non-exclusive, non-transferable license to use the software
**	source code contained in this single source file with hardware products
**	sold by PII. Licensee may distribute binary derivative works using this
**	software and running on PII hardware products to third parties without
**	fee or other restrictions.
**	
**	PII MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
**	SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
**	IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
**	OR NON-INFRINGEMENT. PII SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY
**	LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THE SOFTWARE OR
**	ITS DERIVATIVES.
**	
**	By using or copying this Software, Licensee agrees to abide by the
**	copyright law and all other applicable laws of the U.S. including, but
**	not limited to, export control laws, and the terms of this license. PII
**	shall have the right to terminate this license immediately by written
**	notice upon Licensee's breach of, or non-compliance with, any of its
**	terms. Licensee may be held legally responsible for any copyright
**	infringement or damages resulting from Licensee's failure to abide by
**	the terms of this license. 
**	
\******************************************************************************/

#include	<cfxbios.h>		// Persistor BIOS and I/O Definitions
#include	<cfxpico.h>		// Persistor PicoDOS Definitions

#include	<ctype.h>
#include	<errno.h>
#include	<stdarg.h>
#include	<stddef.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>


/******************************************************************************\
**	TUChangeBaud		Change baud rate of TPUART Rx or Tx
**	returns the previous baud rate
\******************************************************************************/
long TUChangeBaud(TUPort *tup, long baud, bool isTx);
long TUChangeBaud(TUPort *tup, long baud, bool isTx)
	{

	typedef struct
		{			
		void		*rxchp;		// receiver
		void		*txchp;		// transmitter
		} tuport;

	enum 	// UART Parameter RAM Fields
		{
		uartParityTemp,
		uartMatchRate,
		uartRxTxDataReg,
		uartDataSize,
		uartActualBitCount,
		uartShiftRegister
		};

	enum	// UART Host Service Requests
		{
		uartNotUsed0,
		uartNotUsed1,
		uartReceive,
		uartTransmit
		};

	tuport	*tp = (tuport *) tup;
	struct
		{
		void			*port;		// reverse link
		short			slot;		// TPU channel 1-15
		short			priority;	// TPU channel priority (off,lo,mid,hi)
		long			baud;		// BAUD rate
		} *tchp = isTx ? tp->txchp : tp->rxchp;

	long	prevbaud = tchp->baud;

	TPRAM[tchp->slot][uartMatchRate] = 
		(TPUGetTCR1Clock() + (baud / 2)) / baud;
	TPUHostServiceSession(tchp->slot, isTx ? uartTransmit : uartReceive,
		tpuDontChangeHostSequence, tpuDontChangePriority, true);

	return prevbaud;	// previous baud

	}	//____ TUChangeBaud() ____//


/******************************************************************************\
**	DelayNCharAtBaud		Delay for the time N characters will take to 
**							percolate out a buffered uart.
\******************************************************************************/
void DelayNCharAtBaud(uchar charcount, long baud);
void DelayNCharAtBaud(uchar charcount, long baud)
	{
	RTCDelayMicroSeconds(charcount * (1000000 / (baud / 10)));
	}


/******************************************************************************\
**	TUBreak		Transmit a break from the TPUART of duration ms (70ms max)
**	
**	The maximum break time for a TPUART at the standard 4MHz TCR1 clock rate
**	is about 70ms, which is long enough to constitute an RS232 break for baud
**	rates 240 and above.
\******************************************************************************/
void TUBreak(TUPort *tup, uchar ms);
void TUBreak(TUPort *tup, uchar ms)
	{
	long	breakbaud, savebaud, minbaud;
	
	// compute baud rate for 9-bits of zero equal to requested milliseconds
	breakbaud = TPUGetTCR1Clock() / (((TPUGetTCR1Clock() / 1000L) * ms) / 9);
	minbaud = (TPUGetTCR1Clock() / 32767) + 1;
	if (breakbaud < minbaud)
		breakbaud = minbaud;

	savebaud = TUChangeBaud(tup, breakbaud, true);

	TUTxWaitCompletion(tup);	// wait for any prior chars to complete
	DelayNCharAtBaud(2, savebaud);	// plus two possible trailers...
	TUTxPutByte(tup, 0x00, true);	// send the break
	DelayNCharAtBaud(1, breakbaud);	// just the one

	TUChangeBaud(tup, savebaud, true);	// restore to original
	
	}	//____ TUBreak() ____//


#if 0
/******************************************************************************\
**	TUSimpleTermCmd		CIOgetclp
\******************************************************************************/
//char *TUSimpleTermCmd(CmdInfoPtr cip);
//char *TUSimpleTermCmd(CmdInfoPtr )
int main(void)
	{
	TUPort		*tuport;
	short		rxch, txch;
	long		baud = 9600;

	cprintf("\nPersistor CF2 TPU UART terminal example\n");

//
//	INITIALIZE THE TPU UART MODULE
//		This really belongs in your main() function should generally only
//		be called once at the start of your program. Repeat calls to TUInit
//		release any existing TPU UART channels and attempts to use them
//		without reopening will fail or crash the system.
//
	TUInit(calloc, free);		// give TU manager access to our heap

//
//	OPEN THE TPU UART PORT
//		This is the simplest form of opening a port since we implicitly
//		accept all of the default configuration settings defined by the
//		TUChParams structure which is defined in <cfxpico.h> by passing
//		zero for the last parameter. See the TUAltConsoleCmd for an
//		example showing how to supply custom configuration parameters
//		for an open call.
//
//		The Rx and Tx TPU channels are selected by their associated pin
//		number on the CF2's connector C. The actual TPU channels are
//		never directly referenced after opening, and the pin numbers,
//		or well named macros, are much more intuitive. In this case,
//		we're using the pins that connect to the auxiliary RS232
//		driver for connector AUX1 on a fully loaded PicoDAQ2 RecipeCard
//		which is why also setup pins C29 and C30 to enable the MAX3222.

	rxch = TPUChanFromPin(30);	// AUX1 RS232 receiver on PicoDAQ2 board
	txch = TPUChanFromPin(29);	// AUX1 RS232 transmitter on PicoDAQ2 board
	//PIOSet(29);					// enable MAX322 transmitter (/OFF)
	//PIOClear(30);				// enable MAX322 receivers (/EN)

	tuport = TUOpen(rxch, txch, baud, 0);
	if (tuport == 0)
		{
		cprintf("\n!!! Unexpected error opening TU channel, aborting\n");
		goto exitTUST;
		}


//
//	PREPARE FOR POSSIBLE FAST BLOCK TRANSFERS
//		The TPU UARTs only work with interrupt driven queues, but the
//		main SCI UART run in polled mode until explicitly directed to
//		switch to queued interrupt driven mode. We'll do that now in
//		case we get some big block requests from actions like paste
//		or send text files.
 
	SCIRxSetBuffered(true);	// switch SCI to buffered receive mode
	SCITxSetBuffered(true);	// switch SCI to buffered transmit mode

//
//	ENTER THE TERMINAL LOOP
//		For as long a the PBM button isn't pushed (or pin-C39 grounded,
//		we alternately check the TPU UART receive queue for data, and if
//		found, grab it and send it to the main SCI UART for viewing. Then
//		we check the main SCI UART's queue to see if any keyboard data
//		has come in, and if so, grab that and send it out to whatever is
//		on the receiving end of the TPU UART.
//
//		Note that we used the abbreviated console I/O style macro names
//		(defined in <cfxpico.h> instead of the longer and more descriptive
//		actual function names. You are free to use whatever you prefer.
	printf("type ` to send a BREAK...\n");

	while(PIORead(IRQ5))		// run until PBM button pushed
		{
		ushort	cc, ct;
		if (tgetq(tuport))				// we've got data from afar
			cputc(ct = tgetc(tuport));		// grab it and show it

		if (cgetq())					// we've got data from the keyboard
		//	tputc(tuport, cc = cgetc());	// send it off
		
		if ((cc = cgetc()) != '`')
			tputc(tuport, cc);
		else
			{
			TUBreak(tuport, 70);
			cc = 0;
			}
		
		}
exitTUST:
	if (tuport)
		TUClose(tuport);
 // tuport = 0;	// don't need here, but vital if TUClose could be re-called
	return 0;

	}	//____ TUSimpleTermCmd() ____//

#endif


